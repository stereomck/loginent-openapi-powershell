# PowerShell Module for Login Enterprise Public API 

Powershell module for use with Login Enterprise 4.3.12 (v4 api only)

## Preamble
In the beginning, as web-based APIs and specifically RESTful APIs were emerging from the primordial technology ether, Swagger came along and solved a massive issue. 
How does one use this thing, without a masters degree in computer science, in a rapidly changing landscape, and without calling support every few seconds. 
Swagger further defined a standard of presenting a RESTful API to further reduce the learning curve by adding a way to "discover" a REST based API - something that WSDL did for its users.
With this new standard way of describing an API, and its schema, Swagger introduced a way to generate a CLI from a Swagger API spec. In a crowning achievement, swagger donated this IP to the OpenAPI standard. 
You can now generate CLIs in 20+ coding languages from an OpenAPI spec. This is where our story begins...

## Fast Forward
This project is a Proof of concept for the generation of PowerShell module from a openapi.json spec. The goal of this project was NOT to create an incredible PowerShell experience (I will leave that to development), but to see if a minimally functional powershell module could be easily generated and maintained.
While it may run, it is sure to have some issues. Please open issues accordingly, so we can track the delta between automated generation of PowerShell that matches our API, and the actual usability of the PowerShell modules.

This project uses OpenAPI Tools (https://OpenAPITools.org) and specifically openapi-generator-cli (5.0.0 beta for beta powershell support)  to generate the PowerShell module - be aware that PowerShell is newly supported as a target language and as such, there are bumps.

This project only includes the post build module with a wrapper for setting up the environment (env vars, etc) and some fixups from the generated code. Automated testing has not been added (yet), so if you run into an issue please report it.

## Known Issues

### Testing

Barely any at this point. Just starting that now. Pester is included and a set of proper functional/integration tests will be written, but consider this module as barely tested until a test report is included in the repo.

### Deserialization

Because of the way the code is generated, each schema has its own de-serializer (I would expect to be type aware). You will get a container of objects and you will likely have to walk those to get what you are looking for. The easiest way to address this is to just "redim". Look at getting started.

### Powershell Pipelining

Not really there yet. You will have to build and walk objects for now.

### PowerShell Gotchas

#### Environment Setup

##### Assemblies

Some versions of powershell may NOT load the System.Web.Utility assembly that is used for requests, so to be sure, run the following before making a call (add it to your profile, run an init script, whatever you want):

```
Add-Type -AssemblyName System.Web
```

##### Self-Signed Certs

For the most part, powershell 5.1 is ok. The issue there is that we use Invoke-WebRequest for the calls to the API and Psh 5.1 does NOT support -SkipCertificateCheck. Since LoginEnterprise uses self-signed certs, this is something that we need. If you are using Psh 5.1, please set -SkipCertificateCheck to $false and use the following to blanket ignore ssl checks:

```
#SkipCertificateCheck only exists as a option for Powershell 6.1 and above, so we have to deal with this another way
#If Invoke-WebRequest -SkipCertificateCheck does not work, then we ignore. Be sure to set SkipCertificateCheck to false in your $Configuration
if (-not ([System.Management.Automation.PSTypeName]'ServerCertificateValidationCallback').Type)
{
$certCallback = @"
    using System;
    using System.Net;
    using System.Net.Security;
    using System.Security.Cryptography.X509Certificates;
    public class ServerCertificateValidationCallback
    {
        public static void Ignore()
        {
            if(ServicePointManager.ServerCertificateValidationCallback ==null)
            {
                ServicePointManager.ServerCertificateValidationCallback +=
                    delegate
                    (
                        Object obj,
                        X509Certificate certificate,
                        X509Chain chain,
                        SslPolicyErrors errors
                    )
                    {
                        return true;
                    };
            }
        }
    }
"@
    Add-Type $certCallback
 }

[ServerCertificateValidationCallback]::Ignore()
```

##### TLS 1.2

Powershell, by default, sends TLS v1. If you recieve an error: "Invoke-WebRequest : The request was aborted: Could not create SSL/TLS secure channel." then try:

```
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
```

More info: https://blog.pauby.com/post/force-powershell-to-use-tls-1-2/

##### Naming

This module was (largely) generated. The names are not what we would want ultimately, however as this is really a POC of the generation process and if the output is valueable, we are not changing this at this time. If there is value, we will change the namespace...just not right now. Let us know if you find value, and we can get to those changes :)

## Reporting Issues 

If you run into an issue or feature request, please use the Issues portion of the repository to let us know. Please do not use the wiki, we would like the README or code examples to do the talking.

## Docs

In the docs subdir you will find details on the APIs with examples. These will help you understand, but you will not be able to run them as is. Please use the example in Getting Started to model your scripts with.

## Futures

We do want the issues, because the goal is to generate the module and not hand code it. I expect a CLI will come from development at some point, but this gives you something now.
We will track all the changes from generation to functional output with all fixes automated from a clean build. That is all this project is intended to provide at this time.

We will open this project up for pull requests at some point in the near future as well as forking.

# Getting Started

If you have not already, please review Known Issues.

You will need to have created your token (configuration), and you will need to have that token value to use this module.

## Running your first command

Here is what we use to run an initial post build sanity check (be sure you set your environment up per Known Issues):

```
$TOKEN = "YOUR_ACCESS_TOKEN"

#Install/Import Module
Import-Module -Name '.\src\PSOpenAPITools' -Verbose

$Configuration = Get-Configuration

#Set base url
$Configuration["BaseUrl"]= "https://YOUR_SERVER_IP/publicApi"

#See Known Issues
$Configuration["SkipCertificateCheck"] = $false

#Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "Bearer $TOKEN"

$Accounts = Invoke-ConfigurationGetAccounts -OrderBy "username" -Direction "ascending" -Count 999 -Offset 999 -IncludeTotalCount $true -v
```

Your result should be a PSCustomObject (effectively a hashtable):

```
LoginEnterprise-Powershell>$Accounts |fl


items      : {@{id=41ad2687-ffe0-42a9-90aa-f240a9e7635d; username=le-1000; domain=lab; created=2020-10-08T08:42:16.687902Z}, @{id=36e22860-419a-4f59-b426-9c230dfd8ef6; username=le-1001; domain=lab;
             created=2020-10-08T08:42:16.687902Z}, @{id=e4386fdd-b3ec-4bca-a524-70442220441f; username=le-1002; domain=lab; created=2020-10-08T08:42:16.687902Z}, @{id=993d694a-74fb-42f8-9811-4a970f880ca3;
             username=le-1003; domain=lab; created=2020-10-08T08:42:16.687902Z}...}
totalCount : 5000
offset     : 999

```

Now you may walk the table however you wish:

```
#Redim
$Accounts = $Accounts.items

#List
$Accounts

id                                   username domain created
--                                   -------- ------ -------
41ad2687-ffe0-42a9-90aa-f240a9e7635d le-1000  lab    2020-10-08T08:42:16.687902Z
```