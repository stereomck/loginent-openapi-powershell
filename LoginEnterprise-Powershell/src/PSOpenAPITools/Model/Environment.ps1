#
# Login Enterprise
# v4.0 [API Console (Swagger)](/publicApi/swagger/index.html?urls.primaryName=Login%20Enterprise%20API%20v4), [Documentation (ReDoc)](/publicApi/v4/docs/index.html)  v3.0 [API Console (Swagger)](/publicApi/swagger/index.html?urls.primaryName=Login%20Enterprise%20API%20v3), [Documentation (ReDoc)](/publicApi/v3/docs/index.html)  v2.0 [API Console (Swagger)](/publicApi/swagger/index.html?urls.primaryName=Login%20Enterprise%20API%20v2), [Documentation (ReDoc)](/publicApi/v2/docs/index.html)  v1.0 [API Console (Swagger)](/publicApi/swagger/index.html?urls.primaryName=Login%20Enterprise%20API%20v1), [Documentation (ReDoc)](/publicApi/v1/docs/index.html)    The Login Enterprise Public API provides documentation and Swagger per version within the product. For additional information please refer to the [documentation](https://support.loginvsi.com/hc/en-us/articles/360009534760) on our website. 
# Version: 4.0
# Generated by OpenAPI Generator: https://openapi-generator.tech
#

<#
.SYNOPSIS

No summary available.

.DESCRIPTION

Test environment

.PARAMETER ConnectorConfiguration
No description available.
.PARAMETER LauncherGroups
Launcher groups
.PARAMETER AccountGroups
Account groups
.OUTPUTS

Environment<PSCustomObject>
#>

function Initialize-Environment {
    [CmdletBinding()]
    Param (
        [Parameter(Position = 0, ValueFromPipelineByPropertyName = $true)]
        [PSCustomObject]
        ${ConnectorConfiguration},
        [Parameter(Position = 1, ValueFromPipelineByPropertyName = $true)]
        [PSCustomObject[]]
        ${LauncherGroups},
        [Parameter(Position = 2, ValueFromPipelineByPropertyName = $true)]
        [PSCustomObject[]]
        ${AccountGroups}
    )

    Process {
        'Creating PSCustomObject: PSOpenAPITools => Environment' | Write-Debug
        $PSBoundParameters | Out-DebugParameter | Write-Debug


        $PSO = [PSCustomObject]@{
            "connectorConfiguration" = ${ConnectorConfiguration}
            "launcherGroups" = ${LauncherGroups}
            "accountGroups" = ${AccountGroups}
        }


        return $PSO
    }
}

<#
.SYNOPSIS

Convert from JSON to Environment<PSCustomObject>

.DESCRIPTION

Convert from JSON to Environment<PSCustomObject>

.PARAMETER Json

Json object

.OUTPUTS

Environment<PSCustomObject>
#>
function ConvertFrom-JsonToEnvironment {
    Param(
        [AllowEmptyString()]
        [string]$Json
    )

    Process {
        'Converting JSON to PSCustomObject: PSOpenAPITools => Environment' | Write-Debug
        $PSBoundParameters | Out-DebugParameter | Write-Debug

        $JsonParameters = ConvertFrom-Json -InputObject $Json

        # check if Json contains properties not defined in Environment
        $AllProperties = ("connectorConfiguration", "launcherGroups", "accountGroups")
        foreach ($name in $JsonParameters.PsObject.Properties.Name) {
            if (!($AllProperties.Contains($name))) {
                throw "Error! JSON key '$name' not found in the properties: $($AllProperties)"
            }
        }

        if (!([bool]($JsonParameters.PSobject.Properties.name -match "connectorConfiguration"))) { #optional property not found
            $ConnectorConfiguration = $null
        } else {
            $ConnectorConfiguration = $JsonParameters.PSobject.Properties["connectorConfiguration"].value
        }

        if (!([bool]($JsonParameters.PSobject.Properties.name -match "launcherGroups"))) { #optional property not found
            $LauncherGroups = $null
        } else {
            $LauncherGroups = $JsonParameters.PSobject.Properties["launcherGroups"].value
        }

        if (!([bool]($JsonParameters.PSobject.Properties.name -match "accountGroups"))) { #optional property not found
            $AccountGroups = $null
        } else {
            $AccountGroups = $JsonParameters.PSobject.Properties["accountGroups"].value
        }

        $PSO = [PSCustomObject]@{
            "connectorConfiguration" = ${ConnectorConfiguration}
            "launcherGroups" = ${LauncherGroups}
            "accountGroups" = ${AccountGroups}
        }

        return $PSO
    }

}

