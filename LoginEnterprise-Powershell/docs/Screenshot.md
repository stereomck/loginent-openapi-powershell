# Screenshot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **String** | Screenshot id | [optional] [default to null]
**Created** | **System.DateTime** | Created date-time | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-Screenshot  -Id null `
 -Created null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

