# ScriptStatus
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**State** | [**ScriptState**](ScriptState.md) |  | [optional] [default to null]
**Errors** | **String[]** | Script errors | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-ScriptStatus  -State null `
 -Errors null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

