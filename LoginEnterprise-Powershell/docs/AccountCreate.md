# AccountCreate
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Username** | **String** | Username | [default to null]
**DomainId** | **String** | Domain id | [default to null]
**Password** | **String** | Password | [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-AccountCreate  -Username null `
 -DomainId null `
 -Password null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

