# .\Api.ReportConfigurationApi

All URIs are relative to *http://localhost/publicApi*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Invoke-ConfigurationCreateReportConfiguration**](ReportConfigurationApi.md#Invoke-ConfigurationCreateReportConfiguration) | **POST** /v4/tests/{testId}/report-configurations | Create report-configuration
[**Invoke-ConfigurationCreateReportConfigurationThreshold**](ReportConfigurationApi.md#Invoke-ConfigurationCreateReportConfigurationThreshold) | **POST** /v4/tests/{testId}/report-configurations/{configurationId}/thresholds | Create report-configuration app-threshold
[**Invoke-ConfigurationDeleteReportConfiguration**](ReportConfigurationApi.md#Invoke-ConfigurationDeleteReportConfiguration) | **DELETE** /v4/tests/{testId}/report-configurations/{configurationId} | Delete report-configuration
[**Invoke-ConfigurationDeleteReportConfigurationLogo**](ReportConfigurationApi.md#Invoke-ConfigurationDeleteReportConfigurationLogo) | **DELETE** /v4/tests/{testId}/report-configurations/{configurationId}/logo | Delete report-configuration logo
[**Invoke-ConfigurationDeleteReportConfigurationThreshold**](ReportConfigurationApi.md#Invoke-ConfigurationDeleteReportConfigurationThreshold) | **DELETE** /v4/tests/{testId}/report-configurations/{configurationId}/thresholds/{thresholdId} | Delete report-configuration threshold
[**Invoke-ConfigurationDeleteReportConfigurationThresholds**](ReportConfigurationApi.md#Invoke-ConfigurationDeleteReportConfigurationThresholds) | **DELETE** /v4/tests/{testId}/report-configurations/{configurationId}/thresholds | Delete multiple report-configuration thresholds
[**Invoke-ConfigurationDeleteReportConfigurations**](ReportConfigurationApi.md#Invoke-ConfigurationDeleteReportConfigurations) | **DELETE** /v4/tests/{testId}/report-configurations | Delete multiple report-configuration
[**Invoke-ConfigurationGetReportConfiguration**](ReportConfigurationApi.md#Invoke-ConfigurationGetReportConfiguration) | **GET** /v4/tests/{testId}/report-configurations/{configurationId} | Get report-configuration by id
[**Invoke-ConfigurationGetReportConfigurationLogo**](ReportConfigurationApi.md#Invoke-ConfigurationGetReportConfigurationLogo) | **GET** /v4/tests/{testId}/report-configurations/{configurationId}/logo | Get report-configuration logo
[**Invoke-ConfigurationGetReportConfigurations**](ReportConfigurationApi.md#Invoke-ConfigurationGetReportConfigurations) | **GET** /v4/tests/{testId}/report-configurations | Get paginated list of report-configurations
[**Invoke-ConfigurationReplaceReportConfigurationThresholds**](ReportConfigurationApi.md#Invoke-ConfigurationReplaceReportConfigurationThresholds) | **PUT** /v4/tests/{testId}/report-configurations/{configurationId}/thresholds | Replace report-configuration app-thresholds
[**Invoke-ConfigurationRequestReport**](ReportConfigurationApi.md#Invoke-ConfigurationRequestReport) | **POST** /v4/tests/{testId}/report-configurations/{configurationId}/report-request | Request report by report-configuration id
[**Invoke-ConfigurationUpdateReportConfiguration**](ReportConfigurationApi.md#Invoke-ConfigurationUpdateReportConfiguration) | **PUT** /v4/tests/{testId}/report-configurations/{configurationId} | Update report-configuration
[**Invoke-ConfigurationUpdateReportConfigurationLogo**](ReportConfigurationApi.md#Invoke-ConfigurationUpdateReportConfigurationLogo) | **PUT** /v4/tests/{testId}/report-configurations/{configurationId}/logo | Upload report-configuration logo (supported file types: .png/.svg/.jpg/.jpeg)
[**Invoke-ConfigurationUpdateReportConfigurationThreshold**](ReportConfigurationApi.md#Invoke-ConfigurationUpdateReportConfigurationThreshold) | **PUT** /v4/tests/{testId}/report-configurations/{configurationId}/thresholds/{thresholdId} | Update report-configuration threshold


<a name="Invoke-ConfigurationCreateReportConfiguration"></a>
# **Invoke-ConfigurationCreateReportConfiguration**
> ObjectId Invoke-ConfigurationCreateReportConfiguration<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ReportConfigurationCreate] <PSCustomObject><br>

Create report-configuration

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)
$ReportConfigurationCreate = (Initialize-ReportConfigurationCreate-Frequency (Initialize-Frequency) -LatencyThreshold (Initialize-ThresholdUpdate-IsEnabled $false -Value 123) -LoginTimeThreshold (Initialize-ThresholdUpdate-IsEnabled $false -Value 123) -Name "Name_example" -Description "Description_example" -IsEnabled $false -Notification (Initialize-ReportNotification-IsEnabled $false -Recipient "Recipient_example" -Created Get-Date -LastModified Get-Date)) # ReportConfigurationCreate | Report-configuration data

# Create report-configuration
try {
    ObjectId $Result = Invoke-ConfigurationCreateReportConfiguration -TestId $TestId -ReportConfigurationCreate $ReportConfigurationCreate
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationCreateReportConfiguration: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]
 **ReportConfigurationCreate** | [**ReportConfigurationCreate**](ReportConfigurationCreate.md)| Report-configuration data | 

### Return type

[**ObjectId**](ObjectId.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationCreateReportConfigurationThreshold"></a>
# **Invoke-ConfigurationCreateReportConfigurationThreshold**
> ObjectId Invoke-ConfigurationCreateReportConfigurationThreshold<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ConfigurationId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-AppThresholdCreate] <PSCustomObject><br>

Create report-configuration app-threshold

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)
$ConfigurationId = "ConfigurationId_example" # String | Report-configuration id (default to null)
$AppThresholdCreate = (Initialize-AppThresholdCreate-ApplicationId "ApplicationId_example" -Timer "Timer_example" -IsEnabled $false -Value 123) # AppThresholdCreate | App-threshold data

# Create report-configuration app-threshold
try {
    ObjectId $Result = Invoke-ConfigurationCreateReportConfigurationThreshold -TestId $TestId -ConfigurationId $ConfigurationId -AppThresholdCreate $AppThresholdCreate
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationCreateReportConfigurationThreshold: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]
 **ConfigurationId** | [**String**](String.md)| Report-configuration id | [default to null]
 **AppThresholdCreate** | [**AppThresholdCreate**](AppThresholdCreate.md)| App-threshold data | 

### Return type

[**ObjectId**](ObjectId.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationDeleteReportConfiguration"></a>
# **Invoke-ConfigurationDeleteReportConfiguration**
> void Invoke-ConfigurationDeleteReportConfiguration<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ConfigurationId] <PSCustomObject><br>

Delete report-configuration

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)
$ConfigurationId = "ConfigurationId_example" # String | Report-configuration id (default to null)

# Delete report-configuration
try {
    Invoke-ConfigurationDeleteReportConfiguration -TestId $TestId -ConfigurationId $ConfigurationId
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationDeleteReportConfiguration: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]
 **ConfigurationId** | [**String**](String.md)| Report-configuration id | [default to null]

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationDeleteReportConfigurationLogo"></a>
# **Invoke-ConfigurationDeleteReportConfigurationLogo**
> void Invoke-ConfigurationDeleteReportConfigurationLogo<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ConfigurationId] <PSCustomObject><br>

Delete report-configuration logo

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)
$ConfigurationId = "ConfigurationId_example" # String | Report-configuration id (default to null)

# Delete report-configuration logo
try {
    Invoke-ConfigurationDeleteReportConfigurationLogo -TestId $TestId -ConfigurationId $ConfigurationId
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationDeleteReportConfigurationLogo: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]
 **ConfigurationId** | [**String**](String.md)| Report-configuration id | [default to null]

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationDeleteReportConfigurationThreshold"></a>
# **Invoke-ConfigurationDeleteReportConfigurationThreshold**
> void Invoke-ConfigurationDeleteReportConfigurationThreshold<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ConfigurationId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ThresholdId] <PSCustomObject><br>

Delete report-configuration threshold

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)
$ConfigurationId = "ConfigurationId_example" # String | Report-configuration id (default to null)
$ThresholdId = "ThresholdId_example" # String | Threshold id (default to null)

# Delete report-configuration threshold
try {
    Invoke-ConfigurationDeleteReportConfigurationThreshold -TestId $TestId -ConfigurationId $ConfigurationId -ThresholdId $ThresholdId
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationDeleteReportConfigurationThreshold: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]
 **ConfigurationId** | [**String**](String.md)| Report-configuration id | [default to null]
 **ThresholdId** | [**String**](String.md)| Threshold id | [default to null]

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationDeleteReportConfigurationThresholds"></a>
# **Invoke-ConfigurationDeleteReportConfigurationThresholds**
> void Invoke-ConfigurationDeleteReportConfigurationThresholds<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ConfigurationId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-RequestBody] <String[]><br>

Delete multiple report-configuration thresholds

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)
$ConfigurationId = "ConfigurationId_example" # String | Report-configuration id (default to null)
$RequestBody = @("Property_example") # String[] | Threshold ids

# Delete multiple report-configuration thresholds
try {
    Invoke-ConfigurationDeleteReportConfigurationThresholds -TestId $TestId -ConfigurationId $ConfigurationId -RequestBody $RequestBody
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationDeleteReportConfigurationThresholds: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]
 **ConfigurationId** | [**String**](String.md)| Report-configuration id | [default to null]
 **RequestBody** | [**String[]**](String.md)| Threshold ids | 

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationDeleteReportConfigurations"></a>
# **Invoke-ConfigurationDeleteReportConfigurations**
> void Invoke-ConfigurationDeleteReportConfigurations<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-RequestBody] <String[]><br>

Delete multiple report-configuration

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)
$RequestBody = @("Property_example") # String[] | Report-configuration ids

# Delete multiple report-configuration
try {
    Invoke-ConfigurationDeleteReportConfigurations -TestId $TestId -RequestBody $RequestBody
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationDeleteReportConfigurations: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]
 **RequestBody** | [**String[]**](String.md)| Report-configuration ids | 

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationGetReportConfiguration"></a>
# **Invoke-ConfigurationGetReportConfiguration**
> ReportConfiguration Invoke-ConfigurationGetReportConfiguration<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ConfigurationId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Include] <PSCustomObject[]><br>

Get report-configuration by id

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)
$ConfigurationId = "ConfigurationId_example" # String | Report=configuration id (default to null)
$Include = @((Initialize-ReportConfigurationInclude)) # ReportConfigurationInclude[] | Include options (default to ["none"])

# Get report-configuration by id
try {
    ReportConfiguration $Result = Invoke-ConfigurationGetReportConfiguration -TestId $TestId -ConfigurationId $ConfigurationId -Include $Include
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationGetReportConfiguration: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]
 **ConfigurationId** | [**String**](String.md)| Report&#x3D;configuration id | [default to null]
 **Include** | [**ReportConfigurationInclude[]**](ReportConfigurationInclude.md)| Include options | [default to [&quot;none&quot;]]

### Return type

[**ReportConfiguration**](ReportConfiguration.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationGetReportConfigurationLogo"></a>
# **Invoke-ConfigurationGetReportConfigurationLogo**
> void Invoke-ConfigurationGetReportConfigurationLogo<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ConfigurationId] <PSCustomObject><br>

Get report-configuration logo

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)
$ConfigurationId = "ConfigurationId_example" # String | Report-configuration id (default to null)

# Get report-configuration logo
try {
    Invoke-ConfigurationGetReportConfigurationLogo -TestId $TestId -ConfigurationId $ConfigurationId
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationGetReportConfigurationLogo: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]
 **ConfigurationId** | [**String**](String.md)| Report-configuration id | [default to null]

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationGetReportConfigurations"></a>
# **Invoke-ConfigurationGetReportConfigurations**
> ReportConfigurationResultSet Invoke-ConfigurationGetReportConfigurations<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-OrderBy] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Direction] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Count] <Int32><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Offset] <System.Nullable[Int32]><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-IncludeTotalCount] <System.Nullable[Boolean]><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Include] <PSCustomObject[]><br>

Get paginated list of report-configurations

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)
$OrderBy = (Initialize-ReportConfigurationSortKey) # ReportConfigurationSortKey | Sort key (default to null)
$Direction = (Initialize-SortOrder) # SortOrder | Sort direction (default to null)
$Count = 987 # Int32 | Number of records to return (default to null)
$Offset = 987 # Int32 | Start offset (optional) (default to 0)
$IncludeTotalCount = true # Boolean | Include total number of records (optional) (default to false)
$Include = @((Initialize-ReportConfigurationInclude)) # ReportConfigurationInclude[] | Include options (optional) (default to ["none"])

# Get paginated list of report-configurations
try {
    ReportConfigurationResultSet $Result = Invoke-ConfigurationGetReportConfigurations -TestId $TestId -OrderBy $OrderBy -Direction $Direction -Count $Count -Offset $Offset -IncludeTotalCount $IncludeTotalCount -Include $Include
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationGetReportConfigurations: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]
 **OrderBy** | [**ReportConfigurationSortKey**](ReportConfigurationSortKey.md)| Sort key | [default to null]
 **Direction** | [**SortOrder**](SortOrder.md)| Sort direction | [default to null]
 **Count** | **Int32**| Number of records to return | [default to null]
 **Offset** | **Int32**| Start offset | [optional] [default to 0]
 **IncludeTotalCount** | **Boolean**| Include total number of records | [optional] [default to false]
 **Include** | [**ReportConfigurationInclude[]**](ReportConfigurationInclude.md)| Include options | [optional] [default to [&quot;none&quot;]]

### Return type

[**ReportConfigurationResultSet**](ReportConfigurationResultSet.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationReplaceReportConfigurationThresholds"></a>
# **Invoke-ConfigurationReplaceReportConfigurationThresholds**
> void Invoke-ConfigurationReplaceReportConfigurationThresholds<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ConfigurationId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-AppThresholdCreate] <PSCustomObject[]><br>

Replace report-configuration app-thresholds

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)
$ConfigurationId = "ConfigurationId_example" # String | Report-configuration id (default to null)
$AppThresholdCreate = @((Initialize-AppThresholdCreate-ApplicationId "ApplicationId_example" -Timer "Timer_example" -IsEnabled $false -Value 123)) # AppThresholdCreate[] | App-thresholds data

# Replace report-configuration app-thresholds
try {
    Invoke-ConfigurationReplaceReportConfigurationThresholds -TestId $TestId -ConfigurationId $ConfigurationId -AppThresholdCreate $AppThresholdCreate
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationReplaceReportConfigurationThresholds: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]
 **ConfigurationId** | [**String**](String.md)| Report-configuration id | [default to null]
 **AppThresholdCreate** | [**AppThresholdCreate[]**](AppThresholdCreate.md)| App-thresholds data | 

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationRequestReport"></a>
# **Invoke-ConfigurationRequestReport**
> ObjectId Invoke-ConfigurationRequestReport<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ConfigurationId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ReportRequest] <PSCustomObject><br>

Request report by report-configuration id

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)
$ConfigurationId = "ConfigurationId_example" # String | Report-configuration id (default to null)
$ReportRequest = (Initialize-ReportRequest-ReportPeriodStart Get-Date) # ReportRequest | Report-request data

# Request report by report-configuration id
try {
    ObjectId $Result = Invoke-ConfigurationRequestReport -TestId $TestId -ConfigurationId $ConfigurationId -ReportRequest $ReportRequest
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationRequestReport: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]
 **ConfigurationId** | [**String**](String.md)| Report-configuration id | [default to null]
 **ReportRequest** | [**ReportRequest**](ReportRequest.md)| Report-request data | 

### Return type

[**ObjectId**](ObjectId.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationUpdateReportConfiguration"></a>
# **Invoke-ConfigurationUpdateReportConfiguration**
> void Invoke-ConfigurationUpdateReportConfiguration<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ConfigurationId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ReportConfigurationUpdate] <PSCustomObject><br>

Update report-configuration

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)
$ConfigurationId = "ConfigurationId_example" # String | Report-configuration id (default to null)
$ReportConfigurationUpdate = (Initialize-ReportConfigurationUpdate-Name "Name_example" -Description "Description_example" -IsEnabled $false -Notification (Initialize-ReportNotification-IsEnabled $false -Recipient "Recipient_example" -Created Get-Date -LastModified Get-Date)) # ReportConfigurationUpdate | Report-configuration data

# Update report-configuration
try {
    Invoke-ConfigurationUpdateReportConfiguration -TestId $TestId -ConfigurationId $ConfigurationId -ReportConfigurationUpdate $ReportConfigurationUpdate
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationUpdateReportConfiguration: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]
 **ConfigurationId** | [**String**](String.md)| Report-configuration id | [default to null]
 **ReportConfigurationUpdate** | [**ReportConfigurationUpdate**](ReportConfigurationUpdate.md)| Report-configuration data | 

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationUpdateReportConfigurationLogo"></a>
# **Invoke-ConfigurationUpdateReportConfigurationLogo**
> void Invoke-ConfigurationUpdateReportConfigurationLogo<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ConfigurationId] <PSCustomObject><br>

Upload report-configuration logo (supported file types: .png/.svg/.jpg/.jpeg)

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)
$ConfigurationId = "ConfigurationId_example" # String | Report-configuration id (default to null)

# Upload report-configuration logo (supported file types: .png/.svg/.jpg/.jpeg)
try {
    Invoke-ConfigurationUpdateReportConfigurationLogo -TestId $TestId -ConfigurationId $ConfigurationId
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationUpdateReportConfigurationLogo: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]
 **ConfigurationId** | [**String**](String.md)| Report-configuration id | [default to null]

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationUpdateReportConfigurationThreshold"></a>
# **Invoke-ConfigurationUpdateReportConfigurationThreshold**
> void Invoke-ConfigurationUpdateReportConfigurationThreshold<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ConfigurationId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ThresholdId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ThresholdUpdate] <PSCustomObject><br>

Update report-configuration threshold

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)
$ConfigurationId = "ConfigurationId_example" # String | Report-configuration id (default to null)
$ThresholdId = "ThresholdId_example" # String | Threshold id (default to null)
$ThresholdUpdate =  # ThresholdUpdate | Threshold data

# Update report-configuration threshold
try {
    Invoke-ConfigurationUpdateReportConfigurationThreshold -TestId $TestId -ConfigurationId $ConfigurationId -ThresholdId $ThresholdId -ThresholdUpdate $ThresholdUpdate
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationUpdateReportConfigurationThreshold: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]
 **ConfigurationId** | [**String**](String.md)| Report-configuration id | [default to null]
 **ThresholdId** | [**String**](String.md)| Threshold id | [default to null]
 **ThresholdUpdate** | [**ThresholdUpdate**](ThresholdUpdate.md)| Threshold data | 

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

