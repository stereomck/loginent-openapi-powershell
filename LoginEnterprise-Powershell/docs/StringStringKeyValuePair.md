# StringStringKeyValuePair
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Key** | **String** |  | [optional] [default to null]
**Value** | **String** |  | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-StringStringKeyValuePair  -Key null `
 -Value null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

