# WebApplicationUpdate
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **String** |  | [default to null]
**BrowserName** | [**BrowserName**](BrowserName.md) |  | [default to null]
**StartUrl** | **String** | Start URL | [default to null]
**Name** | **String** | Application name | [default to null]
**Description** | **String** | Application description | [optional] [default to null]
**UserName** | **String** | Application user name | [optional] [default to null]
**Password** | **String** | Application password | [optional] [default to null]
**MustUpdatePassword** | **Boolean** | True if password must be updated | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-WebApplicationUpdate  -Type null `
 -BrowserName null `
 -StartUrl null `
 -Name null `
 -Description null `
 -UserName null `
 -Password null `
 -MustUpdatePassword null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

