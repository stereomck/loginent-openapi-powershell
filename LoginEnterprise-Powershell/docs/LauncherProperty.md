# LauncherProperty
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**PropertyId** | **String** | Launcher property id | [optional] [default to null]
**Value** | **String** | Launcher property value | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-LauncherProperty  -PropertyId null `
 -Value null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

