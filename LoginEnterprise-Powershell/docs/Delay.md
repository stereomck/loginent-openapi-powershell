# Delay
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **String** |  | [default to null]
**DelayInSeconds** | **Int32** | Delay in seconds | [optional] [default to null]
**Id** | **String** | Step id | [optional] [default to null]
**IsEnabled** | **Boolean** | Enable step | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-Delay  -Type null `
 -DelayInSeconds null `
 -Id null `
 -IsEnabled null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

