# Workload
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Steps** | [**OneOfAppInvocationDelay[]**](OneOfAppInvocationDelay.md) | Workload step | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-Workload  -Steps null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

