# ApplicationTestUpdate
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **String** |  | [default to null]
**IsEmailEnabled** | **Boolean** | Enable email notification | [default to null]
**EmailRecipient** | **String** | Notification email address | [default to null]
**IncludeSuccessfulApplications** | **Boolean** | Include successful applications in report | [default to null]
**Name** | **String** | Test name | [default to null]
**Description** | **String** | Test description | [optional] [default to null]
**EnvironmentUpdate** | [**EnvironmentUpdate**](EnvironmentUpdate.md) |  | [optional] [default to null]
**Steps** | [**OneOfAppInvocationCreateDelayCreate[]**](OneOfAppInvocationCreateDelayCreate.md) | Workload steps creation data | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-ApplicationTestUpdate  -Type null `
 -IsEmailEnabled null `
 -EmailRecipient null `
 -IncludeSuccessfulApplications null `
 -Name null `
 -Description null `
 -EnvironmentUpdate null `
 -Steps null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

