# .\Api.LauncherGroupApi

All URIs are relative to *http://localhost/publicApi*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Invoke-ConfigurationCreateLauncherGroup**](LauncherGroupApi.md#Invoke-ConfigurationCreateLauncherGroup) | **POST** /v4/launcher-groups | Create launcher-group
[**Invoke-ConfigurationDeleteLauncherGroup**](LauncherGroupApi.md#Invoke-ConfigurationDeleteLauncherGroup) | **DELETE** /v4/launcher-groups/{groupId} | Delete launcher-group
[**Invoke-ConfigurationDeleteLauncherGroups**](LauncherGroupApi.md#Invoke-ConfigurationDeleteLauncherGroups) | **DELETE** /v4/launcher-groups | delete multiple launcher-groups
[**Invoke-ConfigurationGetLauncherGroup**](LauncherGroupApi.md#Invoke-ConfigurationGetLauncherGroup) | **GET** /v4/launcher-groups/{groupId} | Get launcher-group by id
[**Invoke-ConfigurationGetLauncherGroups**](LauncherGroupApi.md#Invoke-ConfigurationGetLauncherGroups) | **GET** /v4/launcher-groups | Get paginated list of launcher-groups
[**Invoke-ConfigurationUpdateLauncherGroup**](LauncherGroupApi.md#Invoke-ConfigurationUpdateLauncherGroup) | **PUT** /v4/launcher-groups/{groupId} | Update launcher-group


<a name="Invoke-ConfigurationCreateLauncherGroup"></a>
# **Invoke-ConfigurationCreateLauncherGroup**
> ObjectId Invoke-ConfigurationCreateLauncherGroup<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-UNKNOWNBASETYPE] <PSCustomObject><br>

Create launcher-group

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$UNKNOWNBASETYPE = TODO # UNKNOWN_BASE_TYPE | Launcher-group data

# Create launcher-group
try {
    ObjectId $Result = Invoke-ConfigurationCreateLauncherGroup -UNKNOWNBASETYPE $UNKNOWNBASETYPE
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationCreateLauncherGroup: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWNBASETYPE** | [**UNKNOWN_BASE_TYPE**](UNKNOWN_BASE_TYPE.md)| Launcher-group data | 

### Return type

[**ObjectId**](ObjectId.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationDeleteLauncherGroup"></a>
# **Invoke-ConfigurationDeleteLauncherGroup**
> void Invoke-ConfigurationDeleteLauncherGroup<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-GroupId] <PSCustomObject><br>

Delete launcher-group

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$GroupId = "GroupId_example" # String | Launcher-group id (default to null)

# Delete launcher-group
try {
    Invoke-ConfigurationDeleteLauncherGroup -GroupId $GroupId
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationDeleteLauncherGroup: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **GroupId** | [**String**](String.md)| Launcher-group id | [default to null]

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationDeleteLauncherGroups"></a>
# **Invoke-ConfigurationDeleteLauncherGroups**
> void Invoke-ConfigurationDeleteLauncherGroups<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-RequestBody] <String[]><br>

delete multiple launcher-groups

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$RequestBody = @("Property_example") # String[] | Launcher-group ids (optional)

# delete multiple launcher-groups
try {
    Invoke-ConfigurationDeleteLauncherGroups -RequestBody $RequestBody
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationDeleteLauncherGroups: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **RequestBody** | [**String[]**](String.md)| Launcher-group ids | [optional] 

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationGetLauncherGroup"></a>
# **Invoke-ConfigurationGetLauncherGroup**
> OneOfLauncherFilterGroupLauncherSelectionGroup Invoke-ConfigurationGetLauncherGroup<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-GroupId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Include] <PSCustomObject[]><br>

Get launcher-group by id

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$GroupId = "GroupId_example" # String | Launcher-group id (default to null)
$Include = @((Initialize-LauncherGroupInclude)) # LauncherGroupInclude[] | Include options (optional) (default to ["none"])

# Get launcher-group by id
try {
    OneOfLauncherFilterGroupLauncherSelectionGroup $Result = Invoke-ConfigurationGetLauncherGroup -GroupId $GroupId -Include $Include
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationGetLauncherGroup: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **GroupId** | [**String**](String.md)| Launcher-group id | [default to null]
 **Include** | [**LauncherGroupInclude[]**](LauncherGroupInclude.md)| Include options | [optional] [default to [&quot;none&quot;]]

### Return type

[**OneOfLauncherFilterGroupLauncherSelectionGroup**](OneOfLauncherFilterGroupLauncherSelectionGroup.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationGetLauncherGroups"></a>
# **Invoke-ConfigurationGetLauncherGroups**
> LauncherGroupResultSet Invoke-ConfigurationGetLauncherGroups<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-OrderBy] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Direction] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Count] <Int32><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Offset] <System.Nullable[Int32]><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-IncludeTotalCount] <System.Nullable[Boolean]><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Include] <PSCustomObject[]><br>

Get paginated list of launcher-groups

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$OrderBy = (Initialize-LauncherGroupSortKey) # LauncherGroupSortKey | Sort key (default to null)
$Direction = (Initialize-SortOrder) # SortOrder | Sort direction (default to null)
$Count = 987 # Int32 | Number of records to return (default to null)
$Offset = 987 # Int32 | Start offset (optional) (default to 0)
$IncludeTotalCount = true # Boolean | Include total number of records (optional) (default to false)
$Include = @((Initialize-LauncherGroupInclude)) # LauncherGroupInclude[] | Include options (optional) (default to ["none"])

# Get paginated list of launcher-groups
try {
    LauncherGroupResultSet $Result = Invoke-ConfigurationGetLauncherGroups -OrderBy $OrderBy -Direction $Direction -Count $Count -Offset $Offset -IncludeTotalCount $IncludeTotalCount -Include $Include
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationGetLauncherGroups: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **OrderBy** | [**LauncherGroupSortKey**](LauncherGroupSortKey.md)| Sort key | [default to null]
 **Direction** | [**SortOrder**](SortOrder.md)| Sort direction | [default to null]
 **Count** | **Int32**| Number of records to return | [default to null]
 **Offset** | **Int32**| Start offset | [optional] [default to 0]
 **IncludeTotalCount** | **Boolean**| Include total number of records | [optional] [default to false]
 **Include** | [**LauncherGroupInclude[]**](LauncherGroupInclude.md)| Include options | [optional] [default to [&quot;none&quot;]]

### Return type

[**LauncherGroupResultSet**](LauncherGroupResultSet.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationUpdateLauncherGroup"></a>
# **Invoke-ConfigurationUpdateLauncherGroup**
> void Invoke-ConfigurationUpdateLauncherGroup<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-GroupId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-UNKNOWNBASETYPE] <PSCustomObject><br>

Update launcher-group

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$GroupId = "GroupId_example" # String | Launcher-group id (default to null)
$UNKNOWNBASETYPE = TODO # UNKNOWN_BASE_TYPE | Launcher-group data

# Update launcher-group
try {
    Invoke-ConfigurationUpdateLauncherGroup -GroupId $GroupId -UNKNOWNBASETYPE $UNKNOWNBASETYPE
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationUpdateLauncherGroup: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **GroupId** | [**String**](String.md)| Launcher-group id | [default to null]
 **UNKNOWNBASETYPE** | [**UNKNOWN_BASE_TYPE**](UNKNOWN_BASE_TYPE.md)| Launcher-group data | 

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

