# .\Api.ScreenshotApi

All URIs are relative to *http://localhost/publicApi*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Invoke-DataGetScreenshotByAppExecution**](ScreenshotApi.md#Invoke-DataGetScreenshotByAppExecution) | **GET** /v4/test-runs/{testRunId}/app-executions/{appExecutionId}/screenshots/{screenshotId} | Get screenshot by app-execution id and screenshot id
[**Invoke-DataGetScreenshotByEvent**](ScreenshotApi.md#Invoke-DataGetScreenshotByEvent) | **GET** /v4/test-runs/{testRunId}/events/{eventId}/screenshots | Get screenshot by event id
[**Invoke-DataGetScreenshotsByAppExecution**](ScreenshotApi.md#Invoke-DataGetScreenshotsByAppExecution) | **GET** /v4/test-runs/{testRunId}/app-executions/{appExecutionId}/screenshots | Get list of screenshot by app-execution id


<a name="Invoke-DataGetScreenshotByAppExecution"></a>
# **Invoke-DataGetScreenshotByAppExecution**
> System.IO.FileInfo Invoke-DataGetScreenshotByAppExecution<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestRunId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-AppExecutionId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ScreenshotId] <String><br>

Get screenshot by app-execution id and screenshot id

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestRunId = "TestRunId_example" # String | Test-run id (default to null)
$AppExecutionId = "AppExecutionId_example" # String | App-execution id (default to null)
$ScreenshotId = "ScreenshotId_example" # String | Screenshot id (default to null)

# Get screenshot by app-execution id and screenshot id
try {
    System.IO.FileInfo $Result = Invoke-DataGetScreenshotByAppExecution -TestRunId $TestRunId -AppExecutionId $AppExecutionId -ScreenshotId $ScreenshotId
} catch {
    Write-Host ("Exception occured when calling Invoke-DataGetScreenshotByAppExecution: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestRunId** | [**String**](String.md)| Test-run id | [default to null]
 **AppExecutionId** | [**String**](String.md)| App-execution id | [default to null]
 **ScreenshotId** | **String**| Screenshot id | [default to null]

### Return type

**System.IO.FileInfo**

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-DataGetScreenshotByEvent"></a>
# **Invoke-DataGetScreenshotByEvent**
> System.IO.FileInfo Invoke-DataGetScreenshotByEvent<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestRunId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-EventId] <PSCustomObject><br>

Get screenshot by event id

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestRunId = "TestRunId_example" # String | Test-run id (default to null)
$EventId = "EventId_example" # String | Event id (default to null)

# Get screenshot by event id
try {
    System.IO.FileInfo $Result = Invoke-DataGetScreenshotByEvent -TestRunId $TestRunId -EventId $EventId
} catch {
    Write-Host ("Exception occured when calling Invoke-DataGetScreenshotByEvent: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestRunId** | [**String**](String.md)| Test-run id | [default to null]
 **EventId** | [**String**](String.md)| Event id | [default to null]

### Return type

**System.IO.FileInfo**

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-DataGetScreenshotsByAppExecution"></a>
# **Invoke-DataGetScreenshotsByAppExecution**
> Screenshot[] Invoke-DataGetScreenshotsByAppExecution<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestRunId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-AppExecutionId] <PSCustomObject><br>

Get list of screenshot by app-execution id

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestRunId = "TestRunId_example" # String | Test-run id (default to null)
$AppExecutionId = "AppExecutionId_example" # String | App-execution id (default to null)

# Get list of screenshot by app-execution id
try {
    Screenshot[] $Result = Invoke-DataGetScreenshotsByAppExecution -TestRunId $TestRunId -AppExecutionId $AppExecutionId
} catch {
    Write-Host ("Exception occured when calling Invoke-DataGetScreenshotsByAppExecution: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestRunId** | [**String**](String.md)| Test-run id | [default to null]
 **AppExecutionId** | [**String**](String.md)| App-execution id | [default to null]

### Return type

[**Screenshot[]**](Screenshot.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

