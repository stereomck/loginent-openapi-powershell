# AppThreshold
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **String** |  | [default to null]
**ApplicationId** | **String** | Application id | [optional] [default to null]
**Timer** | **String** | Application timer | [optional] [default to null]
**Id** | **String** | Threshold id | [optional] [default to null]
**IsEnabled** | **Boolean** | Enable threshold | [optional] [default to null]
**Value** | **Double** | Threshold value | [optional] [default to null]
**LastModified** | **System.DateTime** | Last modified date-time | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-AppThreshold  -Type null `
 -ApplicationId null `
 -Timer null `
 -Id null `
 -IsEnabled null `
 -Value null `
 -LastModified null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

