# AccountFilterGroupCreate
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **String** |  | [default to null]
**Condition** | **String** | Filter condition (Wildcards available: &quot;&quot;?&quot;&quot; and &quot;&quot;*&quot;&quot;) | [default to null]
**Name** | **String** | Account group name | [default to null]
**Description** | **String** | Account group description | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-AccountFilterGroupCreate  -Type null `
 -Condition null `
 -Name null `
 -Description null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
