# ReportConfiguration
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **String** | Report configuration id | [optional] [default to null]
**TestId** | **String** | Test id | [optional] [default to null]
**Name** | **String** | Report configuration name | [optional] [default to null]
**TestName** | **String** | Test name | [optional] [default to null]
**Description** | **String** | Report configuration description | [optional] [default to null]
**Frequency** | [**Frequency**](Frequency.md) |  | [optional] [default to null]
**IsEnabled** | **Boolean** | Enable report configuration | [optional] [default to null]
**Notification** | [**ReportNotification**](ReportNotification.md) |  | [optional] [default to null]
**Thresholds** | [**OneOfAppThresholdSessionThreshold[]**](OneOfAppThresholdSessionThreshold.md) | Report thresholds | [optional] [default to null]
**Created** | **System.DateTime** | Created date-time | [optional] [default to null]
**LastModified** | **System.DateTime** | Last modified date-time | [optional] [default to null]
**LastReportPeriodStart** | **System.DateTime** | Last report period start date-time | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-ReportConfiguration  -Id null `
 -TestId null `
 -Name null `
 -TestName null `
 -Description null `
 -Frequency null `
 -IsEnabled null `
 -Notification null `
 -Thresholds null `
 -Created null `
 -LastModified null `
 -LastReportPeriodStart null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

