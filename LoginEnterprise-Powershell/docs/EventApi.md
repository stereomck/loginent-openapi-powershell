# .\Api.EventApi

All URIs are relative to *http://localhost/publicApi*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Invoke-DataGetEvent**](EventApi.md#Invoke-DataGetEvent) | **GET** /v4/events/{eventId} | Get event by id
[**Invoke-DataGetEvents**](EventApi.md#Invoke-DataGetEvents) | **GET** /v4/test-runs/{testRunId}/events | Get paginated list of events by test-run id
[**Invoke-DataGetEventsByAppExecution**](EventApi.md#Invoke-DataGetEventsByAppExecution) | **GET** /v4/test-runs/{testRunId}/app-executions/{appExecutionId}/events | Get paginated list of events by app-execution id
[**Invoke-DataGetEventsByUserSession**](EventApi.md#Invoke-DataGetEventsByUserSession) | **GET** /v4/test-runs/{testRunId}/user-sessions/{userSessionId}/events | Get paginated list of events by user-session id


<a name="Invoke-DataGetEvent"></a>
# **Invoke-DataGetEvent**
> ModelEvent Invoke-DataGetEvent<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-EventId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Include] <PSCustomObject[]><br>

Get event by id

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$EventId = "EventId_example" # String | Event id (default to null)
$Include = @((Initialize-EventInclude)) # EventInclude[] | Include options (optional) (default to ["none"])

# Get event by id
try {
    ModelEvent $Result = Invoke-DataGetEvent -EventId $EventId -Include $Include
} catch {
    Write-Host ("Exception occured when calling Invoke-DataGetEvent: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **EventId** | [**String**](String.md)| Event id | [default to null]
 **Include** | [**EventInclude[]**](EventInclude.md)| Include options | [optional] [default to [&quot;none&quot;]]

### Return type

[**ModelEvent**](ModelEvent.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-DataGetEvents"></a>
# **Invoke-DataGetEvents**
> EventResultSet Invoke-DataGetEvents<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestRunId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Count] <Int32><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-VarFrom] <System.Nullable[System.DateTime]><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-To] <System.Nullable[System.DateTime]><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Direction] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Offset] <System.Nullable[Int32]><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-IncludeTotalCount] <System.Nullable[Boolean]><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Include] <PSCustomObject[]><br>

Get paginated list of events by test-run id

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestRunId = "TestRunId_example" # String | Test-run id (default to null)
$Count = 987 # Int32 | Number of records to return (default to null)
$VarFrom = Get-Date # System.DateTime | From date-time (optional) (default to null)
$To = Get-Date # System.DateTime | To date-time (optional) (default to null)
$Direction = (Initialize-SortOrder) # SortOrder | Sort direction (optional) (default to null)
$Offset = 987 # Int32 | Start offset (optional) (default to null)
$IncludeTotalCount = true # Boolean | Include total number of records (optional) (default to null)
$Include = @((Initialize-EventInclude)) # EventInclude[] | Include options (optional) (default to ["none"])

# Get paginated list of events by test-run id
try {
    EventResultSet $Result = Invoke-DataGetEvents -TestRunId $TestRunId -Count $Count -VarFrom $VarFrom -To $To -Direction $Direction -Offset $Offset -IncludeTotalCount $IncludeTotalCount -Include $Include
} catch {
    Write-Host ("Exception occured when calling Invoke-DataGetEvents: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestRunId** | [**String**](String.md)| Test-run id | [default to null]
 **Count** | **Int32**| Number of records to return | [default to null]
 **VarFrom** | **System.DateTime**| From date-time | [optional] [default to null]
 **To** | **System.DateTime**| To date-time | [optional] [default to null]
 **Direction** | [**SortOrder**](SortOrder.md)| Sort direction | [optional] [default to null]
 **Offset** | **Int32**| Start offset | [optional] [default to null]
 **IncludeTotalCount** | **Boolean**| Include total number of records | [optional] [default to null]
 **Include** | [**EventInclude[]**](EventInclude.md)| Include options | [optional] [default to [&quot;none&quot;]]

### Return type

[**EventResultSet**](EventResultSet.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-DataGetEventsByAppExecution"></a>
# **Invoke-DataGetEventsByAppExecution**
> EventResultSet Invoke-DataGetEventsByAppExecution<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestRunId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-AppExecutionId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Count] <Int32><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Direction] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Offset] <System.Nullable[Int32]><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-IncludeTotalCount] <System.Nullable[Boolean]><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Include] <PSCustomObject[]><br>

Get paginated list of events by app-execution id

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestRunId = "TestRunId_example" # String | Test-run id (default to null)
$AppExecutionId = "AppExecutionId_example" # String | App-execution id (default to null)
$Count = 987 # Int32 | Number of records to return (default to null)
$Direction = (Initialize-SortOrder) # SortOrder | Sort direction (optional) (default to null)
$Offset = 987 # Int32 | Start offset (optional) (default to null)
$IncludeTotalCount = true # Boolean | Include total number of records (optional) (default to null)
$Include = @() # EventInclude[] | Include options (optional) (default to ["none"])

# Get paginated list of events by app-execution id
try {
    EventResultSet $Result = Invoke-DataGetEventsByAppExecution -TestRunId $TestRunId -AppExecutionId $AppExecutionId -Count $Count -Direction $Direction -Offset $Offset -IncludeTotalCount $IncludeTotalCount -Include $Include
} catch {
    Write-Host ("Exception occured when calling Invoke-DataGetEventsByAppExecution: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestRunId** | [**String**](String.md)| Test-run id | [default to null]
 **AppExecutionId** | [**String**](String.md)| App-execution id | [default to null]
 **Count** | **Int32**| Number of records to return | [default to null]
 **Direction** | [**SortOrder**](SortOrder.md)| Sort direction | [optional] [default to null]
 **Offset** | **Int32**| Start offset | [optional] [default to null]
 **IncludeTotalCount** | **Boolean**| Include total number of records | [optional] [default to null]
 **Include** | [**EventInclude[]**](EventInclude.md)| Include options | [optional] [default to [&quot;none&quot;]]

### Return type

[**EventResultSet**](EventResultSet.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-DataGetEventsByUserSession"></a>
# **Invoke-DataGetEventsByUserSession**
> EventResultSet Invoke-DataGetEventsByUserSession<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestRunId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-UserSessionId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Count] <Int32><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Direction] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Offset] <System.Nullable[Int32]><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-IncludeTotalCount] <System.Nullable[Boolean]><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Include] <PSCustomObject[]><br>

Get paginated list of events by user-session id

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestRunId = "TestRunId_example" # String | Test-run id (default to null)
$UserSessionId = "UserSessionId_example" # String | User-session id (default to null)
$Count = 987 # Int32 | Number of records to return (default to null)
$Direction =  # SortOrder | Sort direction (optional) (default to null)
$Offset = 987 # Int32 | Start offset (optional) (default to null)
$IncludeTotalCount = true # Boolean | Include total number of records (optional) (default to null)
$Include = @() # EventInclude[] | Include options (optional) (default to ["none"])

# Get paginated list of events by user-session id
try {
    EventResultSet $Result = Invoke-DataGetEventsByUserSession -TestRunId $TestRunId -UserSessionId $UserSessionId -Count $Count -Direction $Direction -Offset $Offset -IncludeTotalCount $IncludeTotalCount -Include $Include
} catch {
    Write-Host ("Exception occured when calling Invoke-DataGetEventsByUserSession: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestRunId** | [**String**](String.md)| Test-run id | [default to null]
 **UserSessionId** | [**String**](String.md)| User-session id | [default to null]
 **Count** | **Int32**| Number of records to return | [default to null]
 **Direction** | [**SortOrder**](SortOrder.md)| Sort direction | [optional] [default to null]
 **Offset** | **Int32**| Start offset | [optional] [default to null]
 **IncludeTotalCount** | **Boolean**| Include total number of records | [optional] [default to null]
 **Include** | [**EventInclude[]**](EventInclude.md)| Include options | [optional] [default to [&quot;none&quot;]]

### Return type

[**EventResultSet**](EventResultSet.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

