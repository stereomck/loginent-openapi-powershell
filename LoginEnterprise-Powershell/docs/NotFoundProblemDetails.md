# NotFoundProblemDetails
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NotFound** | **System.Collections.Hashtable** |  | [optional] [readonly] [default to null]
**Type** | **String** |  | [optional] [default to null]
**Title** | **String** |  | [optional] [default to null]
**Status** | **Int32** |  | [optional] [default to null]
**Detail** | **String** |  | [optional] [default to null]
**Instance** | **String** |  | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-NotFoundProblemDetails  -NotFound null `
 -Type null `
 -Title null `
 -Status null `
 -Detail null `
 -Instance null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

