# EventNotificationUpdate
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **String** |  | [default to null]
**TimesExceeded** | **Int32** | Number of times the event occurred | [default to null]
**PeriodDuration** | **Int32** | Time range for calculation | [default to null]
**IsEnabled** | **Boolean** | Enables notification | [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-EventNotificationUpdate  -Type null `
 -TimesExceeded null `
 -PeriodDuration null `
 -IsEnabled null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

