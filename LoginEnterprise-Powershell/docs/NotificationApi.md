# .\Api.NotificationApi

All URIs are relative to *http://localhost/publicApi*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Invoke-ConfigurationCreateContinuousTestNotification**](NotificationApi.md#Invoke-ConfigurationCreateContinuousTestNotification) | **POST** /v4/tests/{testId}/notifications | Create test-notification
[**Invoke-ConfigurationDeleteContinuousTestNotification**](NotificationApi.md#Invoke-ConfigurationDeleteContinuousTestNotification) | **DELETE** /v4/tests/{testId}/notifications/{notificationId} | Delete test-notification
[**Invoke-ConfigurationGetContinuousTestNotifications**](NotificationApi.md#Invoke-ConfigurationGetContinuousTestNotifications) | **GET** /v4/tests/{testId}/notifications | Get list of test-notification
[**Invoke-ConfigurationUpdateContinuousTestNotification**](NotificationApi.md#Invoke-ConfigurationUpdateContinuousTestNotification) | **PUT** /v4/tests/{testId}/notifications/{notificationId} | Update test-notification


<a name="Invoke-ConfigurationCreateContinuousTestNotification"></a>
# **Invoke-ConfigurationCreateContinuousTestNotification**
> void Invoke-ConfigurationCreateContinuousTestNotification<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ThresholdNotificationCreate] <PSCustomObject><br>

Create test-notification

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)
$ThresholdNotificationCreate = (Initialize-ThresholdNotificationCreate-Type "Type_example" -Threshold (Initialize-AppThresholdCreate-ApplicationId "ApplicationId_example" -Timer "Timer_example" -IsEnabled $false -Value 123) -TimesExceeded 123 -PeriodDuration 123 -IsEnabled $false) # ThresholdNotificationCreate | Test-notification date

# Create test-notification
try {
    Invoke-ConfigurationCreateContinuousTestNotification -TestId $TestId -ThresholdNotificationCreate $ThresholdNotificationCreate
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationCreateContinuousTestNotification: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]
 **ThresholdNotificationCreate** | [**ThresholdNotificationCreate**](ThresholdNotificationCreate.md)| Test-notification date | 

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationDeleteContinuousTestNotification"></a>
# **Invoke-ConfigurationDeleteContinuousTestNotification**
> void Invoke-ConfigurationDeleteContinuousTestNotification<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-NotificationId] <PSCustomObject><br>

Delete test-notification

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)
$NotificationId = "NotificationId_example" # String | Test-notification id (default to null)

# Delete test-notification
try {
    Invoke-ConfigurationDeleteContinuousTestNotification -TestId $TestId -NotificationId $NotificationId
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationDeleteContinuousTestNotification: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]
 **NotificationId** | [**String**](String.md)| Test-notification id | [default to null]

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationGetContinuousTestNotifications"></a>
# **Invoke-ConfigurationGetContinuousTestNotifications**
> OneOfThresholdNotificationEventNotification[] Invoke-ConfigurationGetContinuousTestNotifications<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>

Get list of test-notification

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)

# Get list of test-notification
try {
    OneOfThresholdNotificationEventNotification[] $Result = Invoke-ConfigurationGetContinuousTestNotifications -TestId $TestId
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationGetContinuousTestNotifications: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]

### Return type

[**OneOfThresholdNotificationEventNotification[]**](OneOfThresholdNotificationEventNotification.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationUpdateContinuousTestNotification"></a>
# **Invoke-ConfigurationUpdateContinuousTestNotification**
> void Invoke-ConfigurationUpdateContinuousTestNotification<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-NotificationId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-UNKNOWNBASETYPE] <PSCustomObject><br>

Update test-notification

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)
$NotificationId = "NotificationId_example" # String | Test-notification id (default to null)
$UNKNOWNBASETYPE = TODO # UNKNOWN_BASE_TYPE | Test-notification data

# Update test-notification
try {
    Invoke-ConfigurationUpdateContinuousTestNotification -TestId $TestId -NotificationId $NotificationId -UNKNOWNBASETYPE $UNKNOWNBASETYPE
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationUpdateContinuousTestNotification: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]
 **NotificationId** | [**String**](String.md)| Test-notification id | [default to null]
 **UNKNOWNBASETYPE** | [**UNKNOWN_BASE_TYPE**](UNKNOWN_BASE_TYPE.md)| Test-notification data | 

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

