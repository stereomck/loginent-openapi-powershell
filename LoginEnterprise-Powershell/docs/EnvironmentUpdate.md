# EnvironmentUpdate
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ConnectorId** | **String** | Connector id | [default to null]
**ConnectorParameterValues** | [**StringStringKeyValuePair[]**](StringStringKeyValuePair.md) | Connector parameters | [default to null]
**LauncherGroups** | **String[]** | Launcher groups ids | [optional] [default to null]
**AccountGroups** | **String[]** | Account groups ids | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-EnvironmentUpdate  -ConnectorId null `
 -ConnectorParameterValues null `
 -LauncherGroups null `
 -AccountGroups null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

