# Launcher
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **String** | Launcher name | [optional] [default to null]
**SupportedVersion** | **Boolean** | Indicates whether the version of the launcher is supported | [optional] [default to null]
**ActiveSessions** | **Int32** | Number of active sessions | [optional] [default to null]
**CurrentVersion** | **Boolean** | Indicates whether the launcher version is current | [optional] [default to null]
**FirstSeen** | **System.DateTime** | First seen date-time | [optional] [default to null]
**Properties** | [**LauncherProperty[]**](LauncherProperty.md) | Launcher properties | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-Launcher  -Name null `
 -SupportedVersion null `
 -ActiveSessions null `
 -CurrentVersion null `
 -FirstSeen null `
 -Properties null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

