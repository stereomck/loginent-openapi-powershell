# ReportConfigurationUpdate
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **String** | Report configuration name | [default to null]
**Description** | **String** | Report configuration description | [optional] [default to null]
**IsEnabled** | **Boolean** | Enable report configuration | [default to null]
**Notification** | [**ReportNotification**](ReportNotification.md) |  | [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-ReportConfigurationUpdate  -Name null `
 -Description null `
 -IsEnabled null `
 -Notification null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

