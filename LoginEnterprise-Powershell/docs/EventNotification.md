# EventNotification
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **String** |  | [default to null]
**Type** | [**EventNotificationType**](EventNotificationType.md) |  | [optional] [default to null]
**LastModified** | **System.DateTime** | Last modified date-time | [optional] [default to null]
**Id** | **String** | Notification id | [optional] [default to null]
**TimesExceeded** | **Int32** | Number of times the event occurred | [optional] [default to null]
**PeriodDuration** | **Int32** | Time range for calculation | [optional] [default to null]
**CooldownDuration** | **Int32** | Time to pass between notification emails | [optional] [default to null]
**IsEnabled** | **Boolean** | Enables notification | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-EventNotification  -Type null `
 -Type null `
 -LastModified null `
 -Id null `
 -TimesExceeded null `
 -PeriodDuration null `
 -CooldownDuration null `
 -IsEnabled null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

