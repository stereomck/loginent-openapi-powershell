# .\Api.ReportApi

All URIs are relative to *http://localhost/publicApi*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Invoke-DataGetApplicationTestReport**](ReportApi.md#Invoke-DataGetApplicationTestReport) | **GET** /v4/test-runs/{testRunId}/reports | Get application-test report by test-run id
[**Invoke-DataGetContinuousTestReports**](ReportApi.md#Invoke-DataGetContinuousTestReports) | **GET** /v4/tests/{testId}/report-configurations/{configurationId}/reports | Get paginated list of continuous-test reports by configuration id
[**Invoke-DataGetReport**](ReportApi.md#Invoke-DataGetReport) | **GET** /v4/reports/{reportId} | Get report by id


<a name="Invoke-DataGetApplicationTestReport"></a>
# **Invoke-DataGetApplicationTestReport**
> OneOfApplicationTestReportContinuousTestReport Invoke-DataGetApplicationTestReport<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestRunId] <PSCustomObject><br>

Get application-test report by test-run id

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestRunId = "TestRunId_example" # String | Test-run id (default to null)

# Get application-test report by test-run id
try {
    OneOfApplicationTestReportContinuousTestReport $Result = Invoke-DataGetApplicationTestReport -TestRunId $TestRunId
} catch {
    Write-Host ("Exception occured when calling Invoke-DataGetApplicationTestReport: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestRunId** | [**String**](String.md)| Test-run id | [default to null]

### Return type

[**OneOfApplicationTestReportContinuousTestReport**](OneOfApplicationTestReportContinuousTestReport.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-DataGetContinuousTestReports"></a>
# **Invoke-DataGetContinuousTestReports**
> ReportResultSet Invoke-DataGetContinuousTestReports<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ConfigurationId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Count] <Int32><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Direction] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Offset] <System.Nullable[Int32]><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-IncludeTotalCount] <System.Nullable[Boolean]><br>

Get paginated list of continuous-test reports by configuration id

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)
$ConfigurationId = "ConfigurationId_example" # String | Configuration id (default to null)
$Count = 987 # Int32 | Number of records to return (default to null)
$Direction = (Initialize-SortOrder) # SortOrder | Sort direction (optional) (default to null)
$Offset = 987 # Int32 | Start offset (optional) (default to null)
$IncludeTotalCount = true # Boolean | Include total number of records (optional) (default to null)

# Get paginated list of continuous-test reports by configuration id
try {
    ReportResultSet $Result = Invoke-DataGetContinuousTestReports -TestId $TestId -ConfigurationId $ConfigurationId -Count $Count -Direction $Direction -Offset $Offset -IncludeTotalCount $IncludeTotalCount
} catch {
    Write-Host ("Exception occured when calling Invoke-DataGetContinuousTestReports: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]
 **ConfigurationId** | [**String**](String.md)| Configuration id | [default to null]
 **Count** | **Int32**| Number of records to return | [default to null]
 **Direction** | [**SortOrder**](SortOrder.md)| Sort direction | [optional] [default to null]
 **Offset** | **Int32**| Start offset | [optional] [default to null]
 **IncludeTotalCount** | **Boolean**| Include total number of records | [optional] [default to null]

### Return type

[**ReportResultSet**](ReportResultSet.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-DataGetReport"></a>
# **Invoke-DataGetReport**
> OneOfApplicationTestReportContinuousTestReport Invoke-DataGetReport<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ReportId] <PSCustomObject><br>

Get report by id

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$ReportId = "ReportId_example" # String | Report id (default to null)

# Get report by id
try {
    OneOfApplicationTestReportContinuousTestReport $Result = Invoke-DataGetReport -ReportId $ReportId
} catch {
    Write-Host ("Exception occured when calling Invoke-DataGetReport: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ReportId** | [**String**](String.md)| Report id | [default to null]

### Return type

[**OneOfApplicationTestReportContinuousTestReport**](OneOfApplicationTestReportContinuousTestReport.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

