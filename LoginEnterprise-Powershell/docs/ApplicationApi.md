# .\Api.ApplicationApi

All URIs are relative to *http://localhost/publicApi*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Invoke-ConfigurationCreateApplication**](ApplicationApi.md#Invoke-ConfigurationCreateApplication) | **POST** /v4/applications | Create application.
[**Invoke-ConfigurationDeleteApplication**](ApplicationApi.md#Invoke-ConfigurationDeleteApplication) | **DELETE** /v4/applications/{applicationId} | Delete application
[**Invoke-ConfigurationDeleteApplications**](ApplicationApi.md#Invoke-ConfigurationDeleteApplications) | **DELETE** /v4/applications | Delete multiple applications
[**Invoke-ConfigurationGetApplication**](ApplicationApi.md#Invoke-ConfigurationGetApplication) | **GET** /v4/applications/{applicationId} | Get application by id
[**Invoke-ConfigurationGetApplications**](ApplicationApi.md#Invoke-ConfigurationGetApplications) | **GET** /v4/applications | Gets a paginated list of applications
[**Invoke-ConfigurationUpdateApplication**](ApplicationApi.md#Invoke-ConfigurationUpdateApplication) | **PUT** /v4/applications/{applicationId} | Update application


<a name="Invoke-ConfigurationCreateApplication"></a>
# **Invoke-ConfigurationCreateApplication**
> ObjectId Invoke-ConfigurationCreateApplication<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-UNKNOWNBASETYPE] <PSCustomObject><br>

Create application.

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$UNKNOWNBASETYPE = TODO # UNKNOWN_BASE_TYPE | Application data

# Create application.
try {
    ObjectId $Result = Invoke-ConfigurationCreateApplication -UNKNOWNBASETYPE $UNKNOWNBASETYPE
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationCreateApplication: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWNBASETYPE** | [**UNKNOWN_BASE_TYPE**](UNKNOWN_BASE_TYPE.md)| Application data | 

### Return type

[**ObjectId**](ObjectId.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationDeleteApplication"></a>
# **Invoke-ConfigurationDeleteApplication**
> void Invoke-ConfigurationDeleteApplication<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ApplicationId] <PSCustomObject><br>

Delete application

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$ApplicationId = "ApplicationId_example" # String | Application id (default to null)

# Delete application
try {
    Invoke-ConfigurationDeleteApplication -ApplicationId $ApplicationId
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationDeleteApplication: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ApplicationId** | [**String**](String.md)| Application id | [default to null]

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationDeleteApplications"></a>
# **Invoke-ConfigurationDeleteApplications**
> void Invoke-ConfigurationDeleteApplications<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-RequestBody] <String[]><br>

Delete multiple applications

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$RequestBody = @("Property_example") # String[] | Application ids (optional)

# Delete multiple applications
try {
    Invoke-ConfigurationDeleteApplications -RequestBody $RequestBody
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationDeleteApplications: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **RequestBody** | [**String[]**](String.md)| Application ids | [optional] 

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationGetApplication"></a>
# **Invoke-ConfigurationGetApplication**
> OneOfWebApplicationWindowsApplication Invoke-ConfigurationGetApplication<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ApplicationId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Include] <PSCustomObject[]><br>

Get application by id

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$ApplicationId = "ApplicationId_example" # String | Application id (default to null)
$Include = @((Initialize-ApplicationInclude)) # ApplicationInclude[] | Include options (optional) (default to ["none"])

# Get application by id
try {
    OneOfWebApplicationWindowsApplication $Result = Invoke-ConfigurationGetApplication -ApplicationId $ApplicationId -Include $Include
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationGetApplication: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ApplicationId** | [**String**](String.md)| Application id | [default to null]
 **Include** | [**ApplicationInclude[]**](ApplicationInclude.md)| Include options | [optional] [default to [&quot;none&quot;]]

### Return type

[**OneOfWebApplicationWindowsApplication**](OneOfWebApplicationWindowsApplication.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationGetApplications"></a>
# **Invoke-ConfigurationGetApplications**
> ApplicationResultSet Invoke-ConfigurationGetApplications<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-OrderBy] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Direction] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Count] <Int32><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Offset] <System.Nullable[Int32]><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-IncludeTotalCount] <System.Nullable[Boolean]><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Include] <PSCustomObject[]><br>

Gets a paginated list of applications

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$OrderBy = (Initialize-ApplicationSortKey) # ApplicationSortKey | Sort key (default to null)
$Direction = (Initialize-SortOrder) # SortOrder | Sort direction (default to null)
$Count = 987 # Int32 | Number of records to return (default to null)
$Offset = 987 # Int32 | Start offset (optional) (default to 0)
$IncludeTotalCount = true # Boolean | Include total number of records (optional) (default to false)
$Include = @((Initialize-ApplicationInclude)) # ApplicationInclude[] | Include options (optional) (default to ["none"])

# Gets a paginated list of applications
try {
    ApplicationResultSet $Result = Invoke-ConfigurationGetApplications -OrderBy $OrderBy -Direction $Direction -Count $Count -Offset $Offset -IncludeTotalCount $IncludeTotalCount -Include $Include
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationGetApplications: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **OrderBy** | [**ApplicationSortKey**](ApplicationSortKey.md)| Sort key | [default to null]
 **Direction** | [**SortOrder**](SortOrder.md)| Sort direction | [default to null]
 **Count** | **Int32**| Number of records to return | [default to null]
 **Offset** | **Int32**| Start offset | [optional] [default to 0]
 **IncludeTotalCount** | **Boolean**| Include total number of records | [optional] [default to false]
 **Include** | [**ApplicationInclude[]**](ApplicationInclude.md)| Include options | [optional] [default to [&quot;none&quot;]]

### Return type

[**ApplicationResultSet**](ApplicationResultSet.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationUpdateApplication"></a>
# **Invoke-ConfigurationUpdateApplication**
> void Invoke-ConfigurationUpdateApplication<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ApplicationId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-UNKNOWNBASETYPE] <PSCustomObject><br>

Update application

### Example
```powershell
Import-Module -Name 

$Configuration = Get-Configuration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$ApplicationId = "ApplicationId_example" # String | Application id (default to null)
$UNKNOWNBASETYPE = TODO # UNKNOWN_BASE_TYPE | Application data

# Update application
try {
    Invoke-ConfigurationUpdateApplication -ApplicationId $ApplicationId -UNKNOWNBASETYPE $UNKNOWNBASETYPE
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationUpdateApplication: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ApplicationId** | [**String**](String.md)| Application id | [default to null]
 **UNKNOWNBASETYPE** | [**UNKNOWN_BASE_TYPE**](UNKNOWN_BASE_TYPE.md)| Application data | 

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

