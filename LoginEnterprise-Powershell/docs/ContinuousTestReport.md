# ContinuousTestReport
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **String** |  | [default to null]
**ConfigurationId** | **String** | Report configuration id | [optional] [default to null]
**Id** | **String** | Report id | [optional] [default to null]
**TestId** | **String** | Test id | [optional] [default to null]
**OutputContentUri** | **String** | Output content uri | [optional] [default to null]
**State** | [**ReportState**](ReportState.md) |  | [optional] [default to null]
**Trigger** | [**Trigger**](Trigger.md) |  | [optional] [default to null]
**Created** | **System.DateTime** | Created date-time | [optional] [default to null]
**ReportPeriodStart** | **System.DateTime** | Report period start date-time | [optional] [default to null]
**ReportPeriodEnd** | **System.DateTime** | Report period end date-time | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-ContinuousTestReport  -Type null `
 -ConfigurationId null `
 -Id null `
 -TestId null `
 -OutputContentUri null `
 -State null `
 -Trigger null `
 -Created null `
 -ReportPeriodStart null `
 -ReportPeriodEnd null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

