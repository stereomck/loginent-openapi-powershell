# LoadTestRun
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **String** |  | [default to null]
**State** | [**LoadTestState**](LoadTestState.md) |  | [optional] [default to null]
**Result** | [**LoadTestResult**](LoadTestResult.md) |  | [optional] [default to null]
**RampUpCompleted** | **System.DateTime** | Ramp-up completed date-time | [optional] [default to null]
**ActiveSessionCount** | **Int32** | Active session count | [optional] [default to null]
**StatisticsReady** | **Boolean** | Statistics are ready | [optional] [default to null]
**ProductVersion** | **String** | Product version | [optional] [default to null]
**LoginCounts** | [**SuccessCounts**](SuccessCounts.md) |  | [optional] [default to null]
**EngineCounts** | [**SuccessCounts**](SuccessCounts.md) |  | [optional] [default to null]
**AppExecutionCounts** | [**SuccessCounts**](SuccessCounts.md) |  | [optional] [default to null]
**Id** | **String** | Test run id | [optional] [default to null]
**TestId** | **String** | Test id | [optional] [default to null]
**Created** | **System.DateTime** | Created date-time | [optional] [default to null]
**Started** | **System.DateTime** | Started date-time | [optional] [default to null]
**Finished** | **System.DateTime** | Finished date-time | [optional] [default to null]
**Counter** | **Int32** | Test run counter | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-LoadTestRun  -Type null `
 -State null `
 -Result null `
 -RampUpCompleted null `
 -ActiveSessionCount null `
 -StatisticsReady null `
 -ProductVersion null `
 -LoginCounts null `
 -EngineCounts null `
 -AppExecutionCounts null `
 -Id null `
 -TestId null `
 -Created null `
 -Started null `
 -Finished null `
 -Counter null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

