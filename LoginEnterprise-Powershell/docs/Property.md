# Property
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**PropertyId** | **String** | Property id | [optional] [default to null]
**Value** | **String** | Property value | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-Property  -PropertyId null `
 -Value null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

