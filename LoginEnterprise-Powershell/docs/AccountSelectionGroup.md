# AccountSelectionGroup
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **String** |  | [default to null]
**GroupId** | **String** | Account group id | [optional] [default to null]
**Name** | **String** | Account group name | [optional] [default to null]
**MemberCount** | **Int32** | Account group member count | [optional] [default to null]
**Description** | **String** | Account group description | [optional] [default to null]
**Created** | **System.DateTime** | Created date-time | [optional] [default to null]
**LastModified** | **System.DateTime** | Last modified date-time | [optional] [default to null]
**Members** | [**Account[]**](Account.md) | Account group members | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-AccountSelectionGroup  -Type null `
 -GroupId null `
 -Name null `
 -MemberCount null `
 -Description null `
 -Created null `
 -LastModified null `
 -Members null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

