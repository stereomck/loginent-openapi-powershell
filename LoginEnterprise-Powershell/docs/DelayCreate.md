# DelayCreate
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **String** |  | [default to null]
**DelayInSeconds** | **Int32** | Delay in seconds | [default to null]
**IsEnabled** | **Boolean** | Enable step | [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-DelayCreate  -Type null `
 -DelayInSeconds null `
 -IsEnabled null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

