# AccountSelectionGroupCreate
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **String** |  | [default to null]
**MemberIds** | **String[]** | Account group member ids | [default to null]
**Name** | **String** | Account group name | [default to null]
**Description** | **String** | Account group description | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-AccountSelectionGroupCreate  -Type null `
 -MemberIds null `
 -Name null `
 -Description null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
