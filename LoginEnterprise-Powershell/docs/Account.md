# Account
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **String** | Account id | [optional] [default to null]
**Username** | **String** | Account username | [optional] [default to null]
**Domain** | **String** | Account domain | [optional] [default to null]
**Created** | **System.DateTime** | Created date-time | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-Account  -Id null `
 -Username null `
 -Domain null `
 -Created null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)
