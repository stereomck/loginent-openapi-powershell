#
# Login Enterprise
# v4.0 [API Console (Swagger)](/publicApi/swagger/index.html?urls.primaryName=Login%20Enterprise%20API%20v4), [Documentation (ReDoc)](/publicApi/v4/docs/index.html)  v3.0 [API Console (Swagger)](/publicApi/swagger/index.html?urls.primaryName=Login%20Enterprise%20API%20v3), [Documentation (ReDoc)](/publicApi/v3/docs/index.html)  v2.0 [API Console (Swagger)](/publicApi/swagger/index.html?urls.primaryName=Login%20Enterprise%20API%20v2), [Documentation (ReDoc)](/publicApi/v2/docs/index.html)  v1.0 [API Console (Swagger)](/publicApi/swagger/index.html?urls.primaryName=Login%20Enterprise%20API%20v1), [Documentation (ReDoc)](/publicApi/v1/docs/index.html)    The Login Enterprise Public API provides documentation and Swagger per version within the product. For additional information please refer to the [documentation](https://support.loginvsi.com/hc/en-us/articles/360009534760) on our website. 
# Version: 4.0
# Generated by OpenAPI Generator: https://openapi-generator.tech
#

Describe -tag 'PSOpenAPITools' -name 'ReportConfiguration' {
    Context 'ReportConfiguration' {
        It 'Initialize-ReportConfiguration' {
            # a simple test to create an object
            #$NewObject = Initialize-ReportConfiguration -Id "TEST_VALUE" -TestId "TEST_VALUE" -Name "TEST_VALUE" -TestName "TEST_VALUE" -Description "TEST_VALUE" -Frequency "TEST_VALUE" -IsEnabled "TEST_VALUE" -Notification "TEST_VALUE" -Thresholds "TEST_VALUE" -Created "TEST_VALUE" -LastModified "TEST_VALUE" -LastReportPeriodStart "TEST_VALUE"
            #$NewObject | Should -BeOfType ReportConfiguration
            #$NewObject.property | Should -Be 0
        }
    }
}
