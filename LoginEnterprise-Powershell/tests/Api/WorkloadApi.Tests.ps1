#
# Login Enterprise
# v4.0 [API Console (Swagger)](/publicApi/swagger/index.html?urls.primaryName=Login%20Enterprise%20API%20v4), [Documentation (ReDoc)](/publicApi/v4/docs/index.html)  v3.0 [API Console (Swagger)](/publicApi/swagger/index.html?urls.primaryName=Login%20Enterprise%20API%20v3), [Documentation (ReDoc)](/publicApi/v3/docs/index.html)  v2.0 [API Console (Swagger)](/publicApi/swagger/index.html?urls.primaryName=Login%20Enterprise%20API%20v2), [Documentation (ReDoc)](/publicApi/v2/docs/index.html)  v1.0 [API Console (Swagger)](/publicApi/swagger/index.html?urls.primaryName=Login%20Enterprise%20API%20v1), [Documentation (ReDoc)](/publicApi/v1/docs/index.html)    The Login Enterprise Public API provides documentation and Swagger per version within the product. For additional information please refer to the [documentation](https://support.loginvsi.com/hc/en-us/articles/360009534760) on our website. 
# Version: 4.0
# Generated by OpenAPI Generator: https://openapi-generator.tech
#

Describe -tag 'PSOpenAPITools' -name 'WorkloadApi' {
    Context 'Invoke-ConfigurationCreateTestWorkloadSteps' {
        It 'Test Invoke-ConfigurationCreateTestWorkloadSteps' {
            #$TestResult = Invoke-ConfigurationCreateTestWorkloadSteps -TestId "TEST_VALUE" -OneOfAppInvocationCreateDelayCreate "TEST_VALUE"
            #$TestResult | Should -BeOfType TODO
            #$TestResult.property | Should -Be 0
        }
    }

    Context 'Invoke-ConfigurationDeleteTestWorkloadStep' {
        It 'Test Invoke-ConfigurationDeleteTestWorkloadStep' {
            #$TestResult = Invoke-ConfigurationDeleteTestWorkloadStep -TestId "TEST_VALUE" -StepId "TEST_VALUE"
            #$TestResult | Should -BeOfType TODO
            #$TestResult.property | Should -Be 0
        }
    }

    Context 'Invoke-ConfigurationDeleteTestWorkloadSteps' {
        It 'Test Invoke-ConfigurationDeleteTestWorkloadSteps' {
            #$TestResult = Invoke-ConfigurationDeleteTestWorkloadSteps -TestId "TEST_VALUE" -RequestBody "TEST_VALUE"
            #$TestResult | Should -BeOfType TODO
            #$TestResult.property | Should -Be 0
        }
    }

    Context 'Invoke-ConfigurationGetTestWorkload' {
        It 'Test Invoke-ConfigurationGetTestWorkload' {
            #$TestResult = Invoke-ConfigurationGetTestWorkload -TestId "TEST_VALUE"
            #$TestResult | Should -BeOfType TODO
            #$TestResult.property | Should -Be 0
        }
    }

    Context 'Invoke-ConfigurationReplaceTestWorkloadSteps' {
        It 'Test Invoke-ConfigurationReplaceTestWorkloadSteps' {
            #$TestResult = Invoke-ConfigurationReplaceTestWorkloadSteps -TestId "TEST_VALUE" -OneOfAppInvocationCreateDelayCreate "TEST_VALUE"
            #$TestResult | Should -BeOfType TODO
            #$TestResult.property | Should -Be 0
        }
    }

    Context 'Invoke-ConfigurationUpdateTestWorkloadStep' {
        It 'Test Invoke-ConfigurationUpdateTestWorkloadStep' {
            #$TestResult = Invoke-ConfigurationUpdateTestWorkloadStep -TestId "TEST_VALUE" -StepId "TEST_VALUE" -UNKNOWNBASETYPE "TEST_VALUE"
            #$TestResult | Should -BeOfType TODO
            #$TestResult.property | Should -Be 0
        }
    }

}
