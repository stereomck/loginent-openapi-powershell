In Build.ps1, change manifest:
Author = 'Michael Kent'
    CompanyName = 'N/A'
    Description = 'LE-Posh - the PowerShell module for Login Enterprise'
    RootModule = 'LE-Posh.psm1'
    Guid = '{A1697279-F789-428E-9B12-C7C85C394B0D}' # Has to be static, otherwise each new build will be considered different module

    PowerShellVersion = '5.1'

{
  "packageName": "PS-LoginEnterprise"
}

openapi-generator generate -i ..\openapi.json -g powershell -o .\LoginEnterprise_psh -c .\config.json

function generate-powershell($specFile) {
    Write-Host "generating powershell module from spec: $specFile..."
}

function fix-arrays {}

function fix-namespace {}

function ReplaceText($fileInfo)
{
    if( $_.GetType().Name -ne 'FileInfo')
    {
        # i.e. reject DirectoryInfo and other types
         return
    }
    $old = '$AllProperties = ()'
    $new = '$AllProperties = @()'
    #(Get-Content $fileInfo.FullName) | % {$_ -replace $old, $new} | Set-Content -path $fileInfo.FullName
    (Get-Content $fileInfo.FullName).Replace($old,$new) | Set-Content -path $fileInfo.FullName
    "Processed: " + $fileInfo.FullName
}

$loc = 'C:\Users\administrator.LOGINAM\Downloads\lvsi_powershell_clean\src\PSOpenAPITools\Model'
cd $loc
$files = Get-ChildItem . -recurse

$files | % { ReplaceText( $_ ) }

if (!$(Get-Module -Name PSOpenApiTools)){
	Write-Host "Importing Module"
	Import-Module .\PSOpenApiTools.psm1
}



Invoke-V3EnvironmentList -OrderBy "name" -Direction "ascending" -Count 10

# Authenticate using API Secret and Get Access Token
function Set-Authentication {
	# Get Secret from Configuration File
	$Configuration = Get-Configuration

	if (!$Secret) {
	  throw "Error! The required configuration `secret` missing from config file."
	}

	# Create Authentication Request
	$authRequest = Initialize-AuthenticationRequest $Secret

	# Get Access Token
	$authResponse = Invoke-V3AuthenticateSecret $authRequest

	# Set Access Token in Configuration
	Set-Configuration -AccessToken "Bearer $($authResponse.accessToken)"
}
