10.50.2.15 - public API token: _pbfB2LKqI3H6dyEhn-z9pc4EWc2Ljfdg69oNSJxZ5c
curl -X GET "https://10.50.2.15/publicApi/v4/tests?orderBy=name&direction=ascending&count=9&include=none" -H  "accept: application/json" -H  "Authorization: Bearer _pbfB2LKqI3H6dyEhn-z9pc4EWc2Ljfdg69oNSJxZ5c"



$Configuration = Get-Configuration

# Set base url
$Configuration["BaseUrl"]= "https://10.50.2.15/publicApi"

# Disable SSL Cert Checking
$Configuration["SkipCertificateCheck"] = $true

Add-Type -AssemblyName System.Web

if (-not ([System.Management.Automation.PSTypeName]'ServerCertificateValidationCallback').Type)
{
$certCallback = @"
    using System;
    using System.Net;
    using System.Net.Security;
    using System.Security.Cryptography.X509Certificates;
    public class ServerCertificateValidationCallback
    {
        public static void Ignore()
        {
            if(ServicePointManager.ServerCertificateValidationCallback ==null)
            {
                ServicePointManager.ServerCertificateValidationCallback += 
                    delegate
                    (
                        Object obj, 
                        X509Certificate certificate, 
                        X509Chain chain, 
                        SslPolicyErrors errors
                    )
                    {
                        return true;
                    };
            }
        }
    }
"@
    Add-Type $certCallback
 }
[ServerCertificateValidationCallback]::Ignore()

# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "Bearer"

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"

# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "_pbfB2LKqI3H6dyEhn-z9pc4EWc2Ljfdg69oNSJxZ5c"

$Configuration

$OrderBy = "name"
$Direction = "ascending"
$Count = 100
$Offset = 987
$IncludeTotalCount = $True
$Result = Invoke-ConfigurationGetAccounts -OrderBy $OrderBy -Direction $Direction -Count $Count -Offset $Offset -IncludeTotalCount $IncludeTotalCount