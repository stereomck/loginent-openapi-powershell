# LauncherFilterGroup
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **String** |  | [default to null]
**Condition** | **String** | Filter condition (Wildcards available: &quot;&quot;?&quot;&quot; and &quot;&quot;*&quot;&quot;) | [optional] [default to null]
**Id** | **String** | Launcher group id | [optional] [default to null]
**Name** | **String** | Launcher group name | [optional] [default to null]
**MemberCount** | **Int32** | Launcher group member count | [optional] [default to null]
**Description** | **String** | Launcher group description | [optional] [default to null]
**Members** | [**Launcher[]**](Launcher.md) | Launcher group members | [optional] [default to null]
**Created** | **System.DateTime** | Creation date-time | [optional] [default to null]
**LastModified** | **System.DateTime** | Last modified date-time | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-PSOpenAPIToolsLauncherFilterGroup  -Type null `
 -Condition null `
 -Id null `
 -Name null `
 -MemberCount null `
 -Description null `
 -Members null `
 -Created null `
 -LastModified null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

