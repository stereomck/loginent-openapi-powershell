# AppInvocationUpdate
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **String** |  | [default to null]
**IsEnabled** | **Boolean** | Enable step | [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-PSOpenAPIToolsAppInvocationUpdate  -Type null `
 -IsEnabled null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

