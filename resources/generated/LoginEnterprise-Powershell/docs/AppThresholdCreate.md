# AppThresholdCreate
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ApplicationId** | **String** | Application id | [default to null]
**Timer** | **String** | Application timer | [default to null]
**IsEnabled** | **Boolean** | Enable threshold | [default to null]
**Value** | **Double** | Threshold value | [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-PSOpenAPIToolsAppThresholdCreate  -ApplicationId null `
 -Timer null `
 -IsEnabled null `
 -Value null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

