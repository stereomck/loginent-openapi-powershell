# PSOpenAPITools.PSOpenAPITools\Api.EnvironmentApi

All URIs are relative to *http://localhost/publicApi*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Invoke-ConfigurationGetTestEnvironment**](EnvironmentApi.md#Invoke-ConfigurationGetTestEnvironment) | **GET** /v4/tests/{testId}/environment | Get test-environment by id
[**Invoke-ConfigurationUpdateTestEnvironment**](EnvironmentApi.md#Invoke-ConfigurationUpdateTestEnvironment) | **PUT** /v4/tests/{testId}/environment | Update test-environment


<a name="Invoke-ConfigurationGetTestEnvironment"></a>
# **Invoke-ConfigurationGetTestEnvironment**
> Environment Invoke-ConfigurationGetTestEnvironment<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>

Get test-environment by id

### Example
```powershell
Import-Module -Name PSOpenAPITools

$Configuration = Get-PSOpenAPIToolsConfiguration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)

# Get test-environment by id
try {
    Environment $Result = Invoke-ConfigurationGetTestEnvironment -TestId $TestId
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationGetTestEnvironment: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]

### Return type

[**Environment**](Environment.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationUpdateTestEnvironment"></a>
# **Invoke-ConfigurationUpdateTestEnvironment**
> void Invoke-ConfigurationUpdateTestEnvironment<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-EnvironmentUpdate] <PSCustomObject><br>

Update test-environment

### Example
```powershell
Import-Module -Name PSOpenAPITools

$Configuration = Get-PSOpenAPIToolsConfiguration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)
$EnvironmentUpdate = (Initialize-EnvironmentUpdate-ConnectorId "ConnectorId_example" -ConnectorParameterValues @((Initialize-StringStringKeyValuePair-Key "Key_example" -Value "Value_example")) -LauncherGroups @("LauncherGroups_example") -AccountGroups @("AccountGroups_example")) # EnvironmentUpdate | Test-environment data

# Update test-environment
try {
    Invoke-ConfigurationUpdateTestEnvironment -TestId $TestId -EnvironmentUpdate $EnvironmentUpdate
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationUpdateTestEnvironment: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]
 **EnvironmentUpdate** | [**EnvironmentUpdate**](EnvironmentUpdate.md)| Test-environment data | 

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

