# Environment
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ConnectorConfiguration** | [**ConnectorConfiguration**](ConnectorConfiguration.md) |  | [optional] [default to null]
**LauncherGroups** | [**OneOfLauncherFilterGroupLauncherSelectionGroup[]**](OneOfLauncherFilterGroupLauncherSelectionGroup.md) | Launcher groups | [optional] [default to null]
**AccountGroups** | [**OneOfAccountFilterGroupAccountSelectionGroup[]**](OneOfAccountFilterGroupAccountSelectionGroup.md) | Account groups | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-PSOpenAPIToolsEnvironment  -ConnectorConfiguration null `
 -LauncherGroups null `
 -AccountGroups null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

