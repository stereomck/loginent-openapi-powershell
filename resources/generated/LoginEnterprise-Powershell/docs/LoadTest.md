# LoadTest
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **String** |  | [default to null]
**NumberOfSessions** | **Int32** | Number of sessions | [optional] [default to null]
**RampUpDurationInMinutes** | **Int32** | Ramp up duration in minutes | [optional] [default to null]
**TestDurationInMinutes** | **Int32** | Test duration in minutes | [optional] [default to null]
**RampDownDurationInMinutes** | **Int32** | Ramp down duration in minutes | [optional] [default to null]
**State** | [**TestControlState**](TestControlState.md) |  | [optional] [default to null]
**Id** | **String** | Test id | [optional] [default to null]
**Name** | **String** | Test name | [optional] [default to null]
**Description** | **String** | Test description | [optional] [default to null]
**Created** | **System.DateTime** | Created date-time | [optional] [default to null]
**Environment** | [**Environment**](Environment.md) |  | [optional] [default to null]
**Workload** | [**Workload**](Workload.md) |  | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-PSOpenAPIToolsLoadTest  -Type null `
 -NumberOfSessions null `
 -RampUpDurationInMinutes null `
 -TestDurationInMinutes null `
 -RampDownDurationInMinutes null `
 -State null `
 -Id null `
 -Name null `
 -Description null `
 -Created null `
 -Environment null `
 -Workload null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

