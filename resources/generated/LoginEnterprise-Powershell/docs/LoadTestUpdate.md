# LoadTestUpdate
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **String** |  | [default to null]
**NumberOfSessions** | **Int32** | Number of sessions | [default to null]
**RampUpDurationInMinutes** | **Int32** | Ramp up duration in minutes | [default to null]
**TestDurationInMinutes** | **Int32** | Test duration in minutes | [default to null]
**Name** | **String** | Test name | [default to null]
**Description** | **String** | Test description | [optional] [default to null]
**EnvironmentUpdate** | [**EnvironmentUpdate**](EnvironmentUpdate.md) |  | [optional] [default to null]
**Steps** | [**OneOfAppInvocationCreateDelayCreate[]**](OneOfAppInvocationCreateDelayCreate.md) | Workload steps creation data | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-PSOpenAPIToolsLoadTestUpdate  -Type null `
 -NumberOfSessions null `
 -RampUpDurationInMinutes null `
 -TestDurationInMinutes null `
 -Name null `
 -Description null `
 -EnvironmentUpdate null `
 -Steps null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

