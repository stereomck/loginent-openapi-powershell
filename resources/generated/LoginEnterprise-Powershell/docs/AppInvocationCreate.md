# AppInvocationCreate
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **String** |  | [default to null]
**ApplicationId** | **String** | Application id | [default to null]
**IsEnabled** | **Boolean** | Enable step | [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-PSOpenAPIToolsAppInvocationCreate  -Type null `
 -ApplicationId null `
 -IsEnabled null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

