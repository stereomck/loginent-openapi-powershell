# WindowsApplicationUpdate
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **String** |  | [default to null]
**CommandLine** | **String** | Command line arguments | [default to null]
**WorkingDirectory** | **String** | Working directory | [optional] [default to null]
**Name** | **String** | Application name | [default to null]
**Description** | **String** | Application description | [optional] [default to null]
**UserName** | **String** | Application user name | [optional] [default to null]
**Password** | **String** | Application password | [optional] [default to null]
**MustUpdatePassword** | **Boolean** | True if password must be updated | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-PSOpenAPIToolsWindowsApplicationUpdate  -Type null `
 -CommandLine null `
 -WorkingDirectory null `
 -Name null `
 -Description null `
 -UserName null `
 -Password null `
 -MustUpdatePassword null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

