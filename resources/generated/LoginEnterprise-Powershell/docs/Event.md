# ModelEvent
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **String** | Event id | [optional] [default to null]
**EventType** | [**EventType**](EventType.md) |  | [optional] [default to null]
**Created** | **System.DateTime** | Created date-time | [optional] [default to null]
**Title** | **String** | Event title | [optional] [default to null]
**TestId** | **String** | Test id | [optional] [default to null]
**TestRunId** | **String** | Test run id | [optional] [default to null]
**UserSessionId** | **String** | User session id | [optional] [default to null]
**ApplicationId** | **String** | Application id | [optional] [default to null]
**Properties** | [**Property[]**](Property.md) | Event properties | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-PSOpenAPIToolsModelEvent  -Id null `
 -EventType null `
 -Created null `
 -Title null `
 -TestId null `
 -TestRunId null `
 -UserSessionId null `
 -ApplicationId null `
 -Properties null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

