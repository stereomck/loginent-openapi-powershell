# PSOpenAPITools.PSOpenAPITools\Api.TestRunApi

All URIs are relative to *http://localhost/publicApi*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Invoke-DataGetTestRun**](TestRunApi.md#Invoke-DataGetTestRun) | **GET** /v4/test-runs/{testRunId} | Get test-run by id
[**Invoke-DataGetTestRuns**](TestRunApi.md#Invoke-DataGetTestRuns) | **GET** /v4/tests/{testId}/test-runs | Get paginated list of test-runs


<a name="Invoke-DataGetTestRun"></a>
# **Invoke-DataGetTestRun**
> OneOfApplicationTestRunLoadTestRunContinuousTestRun Invoke-DataGetTestRun<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestRunId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Include] <PSCustomObject[]><br>

Get test-run by id

### Example
```powershell
Import-Module -Name PSOpenAPITools

$Configuration = Get-PSOpenAPIToolsConfiguration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestRunId = "TestRunId_example" # String | Test-run id (default to null)
$Include = @((Initialize-TestRunInclude)) # TestRunInclude[] | Include options (optional) (default to ["none"])

# Get test-run by id
try {
    OneOfApplicationTestRunLoadTestRunContinuousTestRun $Result = Invoke-DataGetTestRun -TestRunId $TestRunId -Include $Include
} catch {
    Write-Host ("Exception occured when calling Invoke-DataGetTestRun: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestRunId** | [**String**](String.md)| Test-run id | [default to null]
 **Include** | [**TestRunInclude[]**](TestRunInclude.md)| Include options | [optional] [default to [&quot;none&quot;]]

### Return type

[**OneOfApplicationTestRunLoadTestRunContinuousTestRun**](OneOfApplicationTestRunLoadTestRunContinuousTestRun.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-DataGetTestRuns"></a>
# **Invoke-DataGetTestRuns**
> TestRunResultSet Invoke-DataGetTestRuns<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Direction] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Count] <Int32><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Offset] <System.Nullable[Int32]><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-IncludeTotalCount] <System.Nullable[Boolean]><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Include] <PSCustomObject[]><br>

Get paginated list of test-runs

### Example
```powershell
Import-Module -Name PSOpenAPITools

$Configuration = Get-PSOpenAPIToolsConfiguration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)
$Direction = (Initialize-SortOrder) # SortOrder | Sort direction (default to null)
$Count = 987 # Int32 | Number of records to return (default to null)
$Offset = 987 # Int32 | Start offset (optional) (default to 0)
$IncludeTotalCount = true # Boolean | Include total number of records (optional) (default to false)
$Include = @((Initialize-TestRunInclude)) # TestRunInclude[] | Include options (optional) (default to ["none"])

# Get paginated list of test-runs
try {
    TestRunResultSet $Result = Invoke-DataGetTestRuns -TestId $TestId -Direction $Direction -Count $Count -Offset $Offset -IncludeTotalCount $IncludeTotalCount -Include $Include
} catch {
    Write-Host ("Exception occured when calling Invoke-DataGetTestRuns: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]
 **Direction** | [**SortOrder**](SortOrder.md)| Sort direction | [default to null]
 **Count** | **Int32**| Number of records to return | [default to null]
 **Offset** | **Int32**| Start offset | [optional] [default to 0]
 **IncludeTotalCount** | **Boolean**| Include total number of records | [optional] [default to false]
 **Include** | [**TestRunInclude[]**](TestRunInclude.md)| Include options | [optional] [default to [&quot;none&quot;]]

### Return type

[**TestRunResultSet**](TestRunResultSet.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

