# UserSession
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **String** | User session id | [optional] [default to null]
**TestRunId** | **String** | Test run id | [optional] [default to null]
**TestId** | **String** | Test id | [optional] [default to null]
**Created** | **System.DateTime** | Created date-time | [optional] [default to null]
**LoginState** | [**LoginState**](LoginState.md) |  | [optional] [default to null]
**SessionState** | [**SessionState**](SessionState.md) |  | [optional] [default to null]
**StateLastModified** | **System.DateTime** | State last modified date-time | [optional] [default to null]
**TestType** | [**TestType**](TestType.md) |  | [optional] [default to null]
**AccountId** | **String** | Account id | [optional] [default to null]
**LauncherName** | **String** | Launcher name | [optional] [default to null]
**LoginStartTime** | **System.DateTime** | Login start date-time | [optional] [default to null]
**LoginEndTime** | **System.DateTime** | Login end date-time | [optional] [default to null]
**SessionEndTime** | **System.DateTime** | Session end date-time | [optional] [default to null]
**Properties** | [**Property[]**](Property.md) | Session properties | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-PSOpenAPIToolsUserSession  -Id null `
 -TestRunId null `
 -TestId null `
 -Created null `
 -LoginState null `
 -SessionState null `
 -StateLastModified null `
 -TestType null `
 -AccountId null `
 -LauncherName null `
 -LoginStartTime null `
 -LoginEndTime null `
 -SessionEndTime null `
 -Properties null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

