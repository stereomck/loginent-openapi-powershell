# LoadTestCreate
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **String** |  | [default to null]
**Name** | **String** | Test name | [default to null]
**Description** | **String** | Test description | [optional] [default to null]
**ConnectorId** | **String** | Connector id | [default to null]
**ConnectorParameterValues** | [**StringStringKeyValuePair[]**](StringStringKeyValuePair.md) | Connector parameters | [optional] [default to null]
**AccountGroups** | **String[]** | Account group id | [optional] [default to null]
**LauncherGroups** | **String[]** | Launcher group ids | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-PSOpenAPIToolsLoadTestCreate  -Type null `
 -Name null `
 -Description null `
 -ConnectorId null `
 -ConnectorParameterValues null `
 -AccountGroups null `
 -LauncherGroups null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

