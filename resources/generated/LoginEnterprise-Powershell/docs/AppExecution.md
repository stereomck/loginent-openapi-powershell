# AppExecution
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TestRunId** | **String** | Test run id | [optional] [default to null]
**Id** | **String** | App execution id | [optional] [default to null]
**UserSessionId** | **String** | User session id | [optional] [default to null]
**ApplicationId** | **String** | Application id | [optional] [default to null]
**State** | [**AppExecutionState**](AppExecutionState.md) |  | [optional] [default to null]
**Created** | **System.DateTime** | Created date-time | [optional] [default to null]
**LastModified** | **System.DateTime** | Last modified date-time | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-PSOpenAPIToolsAppExecution  -TestRunId null `
 -Id null `
 -UserSessionId null `
 -ApplicationId null `
 -State null `
 -Created null `
 -LastModified null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

