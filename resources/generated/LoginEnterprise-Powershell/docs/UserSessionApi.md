# PSOpenAPITools.PSOpenAPITools\Api.UserSessionApi

All URIs are relative to *http://localhost/publicApi*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Invoke-DataGetUserSession**](UserSessionApi.md#Invoke-DataGetUserSession) | **GET** /v4/test-runs/{testRunId}/user-sessions/{userSessionId} | Get user-session by id
[**Invoke-DataGetUserSessions**](UserSessionApi.md#Invoke-DataGetUserSessions) | **GET** /v4/test-runs/{testRunId}/user-sessions | Get paginated list of user-sessions


<a name="Invoke-DataGetUserSession"></a>
# **Invoke-DataGetUserSession**
> UserSession Invoke-DataGetUserSession<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestRunId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-UserSessionId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Include] <PSCustomObject[]><br>

Get user-session by id

### Example
```powershell
Import-Module -Name PSOpenAPITools

$Configuration = Get-PSOpenAPIToolsConfiguration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestRunId = "TestRunId_example" # String | Test-run id (default to null)
$UserSessionId = "UserSessionId_example" # String | User-session id (default to null)
$Include = @((Initialize-UserSessionInclude)) # UserSessionInclude[] | Include options (optional) (default to ["none"])

# Get user-session by id
try {
    UserSession $Result = Invoke-DataGetUserSession -TestRunId $TestRunId -UserSessionId $UserSessionId -Include $Include
} catch {
    Write-Host ("Exception occured when calling Invoke-DataGetUserSession: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestRunId** | [**String**](String.md)| Test-run id | [default to null]
 **UserSessionId** | [**String**](String.md)| User-session id | [default to null]
 **Include** | [**UserSessionInclude[]**](UserSessionInclude.md)| Include options | [optional] [default to [&quot;none&quot;]]

### Return type

[**UserSession**](UserSession.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-DataGetUserSessions"></a>
# **Invoke-DataGetUserSessions**
> UserSessionResultSet Invoke-DataGetUserSessions<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestRunId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Count] <Int32><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-VarFrom] <System.Nullable[System.DateTime]><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-To] <System.Nullable[System.DateTime]><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Direction] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Offset] <System.Nullable[Int32]><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-IncludeTotalCount] <System.Nullable[Boolean]><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Include] <PSCustomObject[]><br>

Get paginated list of user-sessions

### Example
```powershell
Import-Module -Name PSOpenAPITools

$Configuration = Get-PSOpenAPIToolsConfiguration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestRunId = "TestRunId_example" # String | Test-run id (default to null)
$Count = 987 # Int32 | Number of records to return (default to null)
$VarFrom = Get-Date # System.DateTime | From date-time (optional) (default to null)
$To = Get-Date # System.DateTime | To date-time (optional) (default to null)
$Direction = (Initialize-SortOrder) # SortOrder | Sort direction (optional) (default to null)
$Offset = 987 # Int32 | Start offset (optional) (default to null)
$IncludeTotalCount = true # Boolean | Include total number of records (optional) (default to null)
$Include = @((Initialize-UserSessionInclude)) # UserSessionInclude[] | Include options (optional) (default to ["none"])

# Get paginated list of user-sessions
try {
    UserSessionResultSet $Result = Invoke-DataGetUserSessions -TestRunId $TestRunId -Count $Count -VarFrom $VarFrom -To $To -Direction $Direction -Offset $Offset -IncludeTotalCount $IncludeTotalCount -Include $Include
} catch {
    Write-Host ("Exception occured when calling Invoke-DataGetUserSessions: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestRunId** | [**String**](String.md)| Test-run id | [default to null]
 **Count** | **Int32**| Number of records to return | [default to null]
 **VarFrom** | **System.DateTime**| From date-time | [optional] [default to null]
 **To** | **System.DateTime**| To date-time | [optional] [default to null]
 **Direction** | [**SortOrder**](SortOrder.md)| Sort direction | [optional] [default to null]
 **Offset** | **Int32**| Start offset | [optional] [default to null]
 **IncludeTotalCount** | **Boolean**| Include total number of records | [optional] [default to null]
 **Include** | [**UserSessionInclude[]**](UserSessionInclude.md)| Include options | [optional] [default to [&quot;none&quot;]]

### Return type

[**UserSessionResultSet**](UserSessionResultSet.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

