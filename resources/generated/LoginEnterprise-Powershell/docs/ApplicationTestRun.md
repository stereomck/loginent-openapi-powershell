# ApplicationTestRun
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **String** |  | [default to null]
**State** | [**ApplicationTestState**](ApplicationTestState.md) |  | [optional] [default to null]
**Result** | [**ApplicationTestResult**](ApplicationTestResult.md) |  | [optional] [default to null]
**AppFailureResults** | [**SuccessCounts**](SuccessCounts.md) |  | [optional] [default to null]
**AppPerformanceResults** | [**SuccessCounts**](SuccessCounts.md) |  | [optional] [default to null]
**Properties** | [**Property[]**](Property.md) | Application test run properties | [optional] [default to null]
**Id** | **String** | Test run id | [optional] [default to null]
**TestId** | **String** | Test id | [optional] [default to null]
**Created** | **System.DateTime** | Created date-time | [optional] [default to null]
**Started** | **System.DateTime** | Started date-time | [optional] [default to null]
**Finished** | **System.DateTime** | Finished date-time | [optional] [default to null]
**Counter** | **Int32** | Test run counter | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-PSOpenAPIToolsApplicationTestRun  -Type null `
 -State null `
 -Result null `
 -AppFailureResults null `
 -AppPerformanceResults null `
 -Properties null `
 -Id null `
 -TestId null `
 -Created null `
 -Started null `
 -Finished null `
 -Counter null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

