# PSOpenAPITools.PSOpenAPITools\Api.MeasurementApi

All URIs are relative to *http://localhost/publicApi*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Invoke-DataGetMeasurements**](MeasurementApi.md#Invoke-DataGetMeasurements) | **GET** /v4/test-runs/{testRunId}/measurements | Get paginated list of measurements by test-run id
[**Invoke-DataGetMeasurementsByAppExecution**](MeasurementApi.md#Invoke-DataGetMeasurementsByAppExecution) | **GET** /v4/test-runs/{testRunId}/app-executions/{appExecutionId}/measurements | Get paginated list of measurements by app-execution id
[**Invoke-DataGetMeasurementsByUserSession**](MeasurementApi.md#Invoke-DataGetMeasurementsByUserSession) | **GET** /v4/test-runs/{testRunId}/user-sessions/{userSessionId}/measurements | Get paginated list of measurements by user-session id


<a name="Invoke-DataGetMeasurements"></a>
# **Invoke-DataGetMeasurements**
> MeasurementResultSet Invoke-DataGetMeasurements<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestRunId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Count] <Int32><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Direction] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Offset] <System.Nullable[Int32]><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-IncludeTotalCount] <System.Nullable[Boolean]><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Include] <PSCustomObject[]><br>

Get paginated list of measurements by test-run id

### Example
```powershell
Import-Module -Name PSOpenAPITools

$Configuration = Get-PSOpenAPIToolsConfiguration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestRunId = "TestRunId_example" # String | Test-run id (default to null)
$Count = 987 # Int32 | Number of records to return (default to null)
$Direction = (Initialize-SortOrder) # SortOrder | Sort direction (optional) (default to null)
$Offset = 987 # Int32 | Start offset (optional) (default to null)
$IncludeTotalCount = true # Boolean | Include total number of records (optional) (default to null)
$Include = @((Initialize-MeasurementInclude)) # MeasurementInclude[] | Include options (optional) (default to ["sessionMeasurements"])

# Get paginated list of measurements by test-run id
try {
    MeasurementResultSet $Result = Invoke-DataGetMeasurements -TestRunId $TestRunId -Count $Count -Direction $Direction -Offset $Offset -IncludeTotalCount $IncludeTotalCount -Include $Include
} catch {
    Write-Host ("Exception occured when calling Invoke-DataGetMeasurements: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestRunId** | [**String**](String.md)| Test-run id | [default to null]
 **Count** | **Int32**| Number of records to return | [default to null]
 **Direction** | [**SortOrder**](SortOrder.md)| Sort direction | [optional] [default to null]
 **Offset** | **Int32**| Start offset | [optional] [default to null]
 **IncludeTotalCount** | **Boolean**| Include total number of records | [optional] [default to null]
 **Include** | [**MeasurementInclude[]**](MeasurementInclude.md)| Include options | [optional] [default to [&quot;sessionMeasurements&quot;]]

### Return type

[**MeasurementResultSet**](MeasurementResultSet.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-DataGetMeasurementsByAppExecution"></a>
# **Invoke-DataGetMeasurementsByAppExecution**
> MeasurementResultSet Invoke-DataGetMeasurementsByAppExecution<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestRunId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-AppExecutionId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Count] <Int32><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Direction] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Offset] <System.Nullable[Int32]><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-IncludeTotalCount] <System.Nullable[Boolean]><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Include] <PSCustomObject[]><br>

Get paginated list of measurements by app-execution id

### Example
```powershell
Import-Module -Name PSOpenAPITools

$Configuration = Get-PSOpenAPIToolsConfiguration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestRunId = "TestRunId_example" # String | Test-run id (default to null)
$AppExecutionId = "AppExecutionId_example" # String | App-execution id (default to null)
$Count = 987 # Int32 | Number of records to return (default to null)
$Direction = (Initialize-SortOrder) # SortOrder | Sort direction (optional) (default to null)
$Offset = 987 # Int32 | Start offset (optional) (default to null)
$IncludeTotalCount = true # Boolean | Include total number of records (optional) (default to null)
$Include = @((Initialize-MeasurementInclude)) # MeasurementInclude[] | Include options (optional) (default to ["sessionMeasurements"])

# Get paginated list of measurements by app-execution id
try {
    MeasurementResultSet $Result = Invoke-DataGetMeasurementsByAppExecution -TestRunId $TestRunId -AppExecutionId $AppExecutionId -Count $Count -Direction $Direction -Offset $Offset -IncludeTotalCount $IncludeTotalCount -Include $Include
} catch {
    Write-Host ("Exception occured when calling Invoke-DataGetMeasurementsByAppExecution: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestRunId** | [**String**](String.md)| Test-run id | [default to null]
 **AppExecutionId** | [**String**](String.md)| App-execution id | [default to null]
 **Count** | **Int32**| Number of records to return | [default to null]
 **Direction** | [**SortOrder**](SortOrder.md)| Sort direction | [optional] [default to null]
 **Offset** | **Int32**| Start offset | [optional] [default to null]
 **IncludeTotalCount** | **Boolean**| Include total number of records | [optional] [default to null]
 **Include** | [**MeasurementInclude[]**](MeasurementInclude.md)| Include options | [optional] [default to [&quot;sessionMeasurements&quot;]]

### Return type

[**MeasurementResultSet**](MeasurementResultSet.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-DataGetMeasurementsByUserSession"></a>
# **Invoke-DataGetMeasurementsByUserSession**
> MeasurementResultSet Invoke-DataGetMeasurementsByUserSession<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestRunId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-UserSessionId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Count] <Int32><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Direction] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Offset] <System.Nullable[Int32]><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-IncludeTotalCount] <System.Nullable[Boolean]><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Include] <PSCustomObject[]><br>

Get paginated list of measurements by user-session id

### Example
```powershell
Import-Module -Name PSOpenAPITools

$Configuration = Get-PSOpenAPIToolsConfiguration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestRunId = "TestRunId_example" # String | Test-run id (default to null)
$UserSessionId = "UserSessionId_example" # String | User-session id (default to null)
$Count = 987 # Int32 | Number of records to return (default to null)
$Direction =  # SortOrder | Sort direction (optional) (default to null)
$Offset = 987 # Int32 | Start offset (optional) (default to null)
$IncludeTotalCount = true # Boolean | Include total number of records (optional) (default to null)
$Include = @() # MeasurementInclude[] | Include options (optional) (default to ["sessionMeasurements"])

# Get paginated list of measurements by user-session id
try {
    MeasurementResultSet $Result = Invoke-DataGetMeasurementsByUserSession -TestRunId $TestRunId -UserSessionId $UserSessionId -Count $Count -Direction $Direction -Offset $Offset -IncludeTotalCount $IncludeTotalCount -Include $Include
} catch {
    Write-Host ("Exception occured when calling Invoke-DataGetMeasurementsByUserSession: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestRunId** | [**String**](String.md)| Test-run id | [default to null]
 **UserSessionId** | [**String**](String.md)| User-session id | [default to null]
 **Count** | **Int32**| Number of records to return | [default to null]
 **Direction** | [**SortOrder**](SortOrder.md)| Sort direction | [optional] [default to null]
 **Offset** | **Int32**| Start offset | [optional] [default to null]
 **IncludeTotalCount** | **Boolean**| Include total number of records | [optional] [default to null]
 **Include** | [**MeasurementInclude[]**](MeasurementInclude.md)| Include options | [optional] [default to [&quot;sessionMeasurements&quot;]]

### Return type

[**MeasurementResultSet**](MeasurementResultSet.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

