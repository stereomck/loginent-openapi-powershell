# ContinuousTest
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **String** |  | [default to null]
**ScheduleType** | [**ContinuousScheduleType**](ContinuousScheduleType.md) |  | [optional] [default to null]
**ScheduleIntervalInMinutes** | **Int32** | Schedule interval in minutes | [optional] [default to null]
**NumberOfSessions** | **Int32** | Number of sessions | [optional] [default to null]
**TakeScriptScreenshots** | **Boolean** | Enable script screenshots | [optional] [default to null]
**RepeatCount** | **Int32** | Number of times the schedule is repeated | [optional] [default to null]
**IsRepeatEnabled** | **Boolean** | Enable schedule repeating | [optional] [default to null]
**IsEnabled** | **Boolean** | Enable schedule | [optional] [default to null]
**RestartOnComplete** | **Boolean** | Enable restarting on completion | [optional] [default to null]
**AlertConfigurations** | [**OneOfThresholdNotificationEventNotification[]**](OneOfThresholdNotificationEventNotification.md) | Alert configurations | [optional] [default to null]
**Id** | **String** | Test id | [optional] [default to null]
**Name** | **String** | Test name | [optional] [default to null]
**Description** | **String** | Test description | [optional] [default to null]
**Created** | **System.DateTime** | Created date-time | [optional] [default to null]
**Environment** | [**Environment**](Environment.md) |  | [optional] [default to null]
**Workload** | [**Workload**](Workload.md) |  | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-PSOpenAPIToolsContinuousTest  -Type null `
 -ScheduleType null `
 -ScheduleIntervalInMinutes null `
 -NumberOfSessions null `
 -TakeScriptScreenshots null `
 -RepeatCount null `
 -IsRepeatEnabled null `
 -IsEnabled null `
 -RestartOnComplete null `
 -AlertConfigurations null `
 -Id null `
 -Name null `
 -Description null `
 -Created null `
 -Environment null `
 -Workload null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

