# PSOpenAPITools.PSOpenAPITools\Api.ThresholdApi

All URIs are relative to *http://localhost/publicApi*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Invoke-ConfigurationCreateApplicationTestThreshold**](ThresholdApi.md#Invoke-ConfigurationCreateApplicationTestThreshold) | **POST** /v4/tests/{testId}/thresholds | Create threshold
[**Invoke-ConfigurationDeleteApplicationTestThreshold**](ThresholdApi.md#Invoke-ConfigurationDeleteApplicationTestThreshold) | **DELETE** /v4/tests/{testId}/thresholds/{thresholdId} | Delete threshold
[**Invoke-ConfigurationGetApplicationTestThresholds**](ThresholdApi.md#Invoke-ConfigurationGetApplicationTestThresholds) | **GET** /v4/tests/{testId}/thresholds | Get list of thresholds by test id
[**Invoke-ConfigurationUpdateApplicationTestThreshold**](ThresholdApi.md#Invoke-ConfigurationUpdateApplicationTestThreshold) | **PUT** /v4/tests/{testId}/thresholds/{thresholdId} | Update threshold


<a name="Invoke-ConfigurationCreateApplicationTestThreshold"></a>
# **Invoke-ConfigurationCreateApplicationTestThreshold**
> ObjectId Invoke-ConfigurationCreateApplicationTestThreshold<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-AppThresholdCreate] <PSCustomObject><br>

Create threshold

### Example
```powershell
Import-Module -Name PSOpenAPITools

$Configuration = Get-PSOpenAPIToolsConfiguration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)
$AppThresholdCreate = (Initialize-AppThresholdCreate-ApplicationId "ApplicationId_example" -Timer "Timer_example" -IsEnabled $false -Value 123) # AppThresholdCreate | Threshold data

# Create threshold
try {
    ObjectId $Result = Invoke-ConfigurationCreateApplicationTestThreshold -TestId $TestId -AppThresholdCreate $AppThresholdCreate
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationCreateApplicationTestThreshold: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]
 **AppThresholdCreate** | [**AppThresholdCreate**](AppThresholdCreate.md)| Threshold data | 

### Return type

[**ObjectId**](ObjectId.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationDeleteApplicationTestThreshold"></a>
# **Invoke-ConfigurationDeleteApplicationTestThreshold**
> void Invoke-ConfigurationDeleteApplicationTestThreshold<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ThresholdId] <PSCustomObject><br>

Delete threshold

### Example
```powershell
Import-Module -Name PSOpenAPITools

$Configuration = Get-PSOpenAPIToolsConfiguration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)
$ThresholdId = "ThresholdId_example" # String | Threshold id (default to null)

# Delete threshold
try {
    Invoke-ConfigurationDeleteApplicationTestThreshold -TestId $TestId -ThresholdId $ThresholdId
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationDeleteApplicationTestThreshold: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]
 **ThresholdId** | [**String**](String.md)| Threshold id | [default to null]

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationGetApplicationTestThresholds"></a>
# **Invoke-ConfigurationGetApplicationTestThresholds**
> OneOfAppThresholdSessionThreshold[] Invoke-ConfigurationGetApplicationTestThresholds<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>

Get list of thresholds by test id

### Example
```powershell
Import-Module -Name PSOpenAPITools

$Configuration = Get-PSOpenAPIToolsConfiguration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)

# Get list of thresholds by test id
try {
    OneOfAppThresholdSessionThreshold[] $Result = Invoke-ConfigurationGetApplicationTestThresholds -TestId $TestId
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationGetApplicationTestThresholds: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]

### Return type

[**OneOfAppThresholdSessionThreshold[]**](OneOfAppThresholdSessionThreshold.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationUpdateApplicationTestThreshold"></a>
# **Invoke-ConfigurationUpdateApplicationTestThreshold**
> void Invoke-ConfigurationUpdateApplicationTestThreshold<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ThresholdId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ThresholdUpdate] <PSCustomObject><br>

Update threshold

### Example
```powershell
Import-Module -Name PSOpenAPITools

$Configuration = Get-PSOpenAPIToolsConfiguration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)
$ThresholdId = "ThresholdId_example" # String | Threshold id (default to null)
$ThresholdUpdate = (Initialize-ThresholdUpdate-IsEnabled $false -Value 123) # ThresholdUpdate | Threshold data

# Update threshold
try {
    Invoke-ConfigurationUpdateApplicationTestThreshold -TestId $TestId -ThresholdId $ThresholdId -ThresholdUpdate $ThresholdUpdate
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationUpdateApplicationTestThreshold: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]
 **ThresholdId** | [**String**](String.md)| Threshold id | [default to null]
 **ThresholdUpdate** | [**ThresholdUpdate**](ThresholdUpdate.md)| Threshold data | 

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

