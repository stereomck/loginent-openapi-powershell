# ThresholdNotification
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **String** |  | [default to null]
**Threshold** | [**OneOfAppThresholdSessionThreshold**](OneOfAppThresholdSessionThreshold.md) | Threshold | [optional] [default to null]
**Id** | **String** | Notification id | [optional] [default to null]
**TimesExceeded** | **Int32** | Number of times the event occurred | [optional] [default to null]
**PeriodDuration** | **Int32** | Time range for calculation | [optional] [default to null]
**CooldownDuration** | **Int32** | Time to pass between notification emails | [optional] [default to null]
**IsEnabled** | **Boolean** | Enables notification | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-PSOpenAPIToolsThresholdNotification  -Type null `
 -Threshold null `
 -Id null `
 -TimesExceeded null `
 -PeriodDuration null `
 -CooldownDuration null `
 -IsEnabled null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

