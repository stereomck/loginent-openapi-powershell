# PSOpenAPITools.PSOpenAPITools\Api.ScriptApi

All URIs are relative to *http://localhost/publicApi*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Invoke-ConfigurationCreateScript**](ScriptApi.md#Invoke-ConfigurationCreateScript) | **POST** /v4/applications/{applicationId}/script | Create script
[**Invoke-ConfigurationGetScript**](ScriptApi.md#Invoke-ConfigurationGetScript) | **GET** /v4/applications/{applicationId}/script | Get script content by application id
[**Invoke-ConfigurationGetScriptStatus**](ScriptApi.md#Invoke-ConfigurationGetScriptStatus) | **GET** /v4/applications/{applicationId}/script/status | Get current/last script status by application id


<a name="Invoke-ConfigurationCreateScript"></a>
# **Invoke-ConfigurationCreateScript**
> void Invoke-ConfigurationCreateScript<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ApplicationId] <PSCustomObject><br>

Create script

### Example
```powershell
Import-Module -Name PSOpenAPITools

$Configuration = Get-PSOpenAPIToolsConfiguration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$ApplicationId = "ApplicationId_example" # String | Application id (default to null)

# Create script
try {
    Invoke-ConfigurationCreateScript -ApplicationId $ApplicationId
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationCreateScript: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ApplicationId** | [**String**](String.md)| Application id | [default to null]

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationGetScript"></a>
# **Invoke-ConfigurationGetScript**
> String Invoke-ConfigurationGetScript<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ApplicationId] <PSCustomObject><br>

Get script content by application id

### Example
```powershell
Import-Module -Name PSOpenAPITools

$Configuration = Get-PSOpenAPIToolsConfiguration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$ApplicationId = "ApplicationId_example" # String | Application id (default to null)

# Get script content by application id
try {
    String $Result = Invoke-ConfigurationGetScript -ApplicationId $ApplicationId
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationGetScript: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ApplicationId** | [**String**](String.md)| Application id | [default to null]

### Return type

**String**

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationGetScriptStatus"></a>
# **Invoke-ConfigurationGetScriptStatus**
> ScriptStatus Invoke-ConfigurationGetScriptStatus<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-ApplicationId] <PSCustomObject><br>

Get current/last script status by application id

### Example
```powershell
Import-Module -Name PSOpenAPITools

$Configuration = Get-PSOpenAPIToolsConfiguration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$ApplicationId = "ApplicationId_example" # String | Application id (default to null)

# Get current/last script status by application id
try {
    ScriptStatus $Result = Invoke-ConfigurationGetScriptStatus -ApplicationId $ApplicationId
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationGetScriptStatus: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ApplicationId** | [**String**](String.md)| Application id | [default to null]

### Return type

[**ScriptStatus**](ScriptStatus.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

