# ApplicationTest
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **String** |  | [default to null]
**IsEmailEnabled** | **Boolean** | Enable email notification | [optional] [default to null]
**EmailRecipient** | **String** | Notification email address | [optional] [default to null]
**IncludeSuccessfulApplications** | **Boolean** | Include successful applications in report | [optional] [default to null]
**State** | [**TestControlState**](TestControlState.md) |  | [optional] [default to null]
**Thresholds** | [**OneOfAppThresholdSessionThreshold[]**](OneOfAppThresholdSessionThreshold.md) | Application test thresholds | [optional] [default to null]
**Id** | **String** | Test id | [optional] [default to null]
**Name** | **String** | Test name | [optional] [default to null]
**Description** | **String** | Test description | [optional] [default to null]
**Created** | **System.DateTime** | Created date-time | [optional] [default to null]
**Environment** | [**Environment**](Environment.md) |  | [optional] [default to null]
**Workload** | [**Workload**](Workload.md) |  | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-PSOpenAPIToolsApplicationTest  -Type null `
 -IsEmailEnabled null `
 -EmailRecipient null `
 -IncludeSuccessfulApplications null `
 -State null `
 -Thresholds null `
 -Id null `
 -Name null `
 -Description null `
 -Created null `
 -Environment null `
 -Workload null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

