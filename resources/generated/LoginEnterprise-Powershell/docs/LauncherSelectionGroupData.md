# LauncherSelectionGroupData
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **String** |  | [default to null]
**LauncherNames** | **String[]** | Launcher names | [optional] [default to null]
**Name** | **String** | Launcher group name | [default to null]
**Description** | **String** | Launcher group description | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-PSOpenAPIToolsLauncherSelectionGroupData  -Type null `
 -LauncherNames null `
 -Name null `
 -Description null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

