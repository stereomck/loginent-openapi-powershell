# ReportRequest
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ReportPeriodStart** | **System.DateTime** | Report period start date-time | [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-PSOpenAPIToolsReportRequest  -ReportPeriodStart null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

