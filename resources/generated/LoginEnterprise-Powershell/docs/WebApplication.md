# WebApplication
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **String** |  | [default to null]
**BrowserName** | [**BrowserName**](BrowserName.md) |  | [optional] [default to null]
**StartUrl** | **String** | Start URL | [optional] [default to null]
**Id** | **String** | Application id | [optional] [default to null]
**Name** | **String** | Application name | [optional] [default to null]
**Description** | **String** | Application description | [optional] [default to null]
**UserName** | **String** | Application user name | [optional] [default to null]
**Created** | **System.DateTime** | Created date-time | [optional] [default to null]
**LastModified** | **System.DateTime** | Last modified date-time | [optional] [default to null]
**Script** | **String** | Application script | [optional] [default to null]
**Timers** | **String[]** | Application timers | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-PSOpenAPIToolsWebApplication  -Type null `
 -BrowserName null `
 -StartUrl null `
 -Id null `
 -Name null `
 -Description null `
 -UserName null `
 -Created null `
 -LastModified null `
 -Script null `
 -Timers null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

