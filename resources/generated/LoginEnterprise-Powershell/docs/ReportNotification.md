# ReportNotification
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**IsEnabled** | **Boolean** | Enable report notification | [optional] [default to null]
**Recipient** | **String** | Notification email address | [optional] [default to null]
**Created** | **System.DateTime** | Created date-time | [optional] [default to null]
**LastModified** | **System.DateTime** | Last modified date-time | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-PSOpenAPIToolsReportNotification  -IsEnabled null `
 -Recipient null `
 -Created null `
 -LastModified null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

