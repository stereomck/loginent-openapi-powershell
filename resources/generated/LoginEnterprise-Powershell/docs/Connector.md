# Connector
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **String** | Connector id | [optional] [default to null]
**Name** | **String** | Connector name | [optional] [default to null]
**Parameters** | [**ConnectorParameter[]**](ConnectorParameter.md) | Connector parameters | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-PSOpenAPIToolsConnector  -Id null `
 -Name null `
 -Parameters null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

