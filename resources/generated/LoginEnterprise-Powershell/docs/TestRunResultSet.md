# TestRunResultSet
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Items** | [**OneOfApplicationTestRunLoadTestRunContinuousTestRun[]**](OneOfApplicationTestRunLoadTestRunContinuousTestRun.md) | Requested items | [optional] [default to null]
**TotalCount** | **Int32** | Total item count (if requested) | [optional] [default to null]
**Offset** | **Int32** | Offset requested | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-PSOpenAPIToolsTestRunResultSet  -Items null `
 -TotalCount null `
 -Offset null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

