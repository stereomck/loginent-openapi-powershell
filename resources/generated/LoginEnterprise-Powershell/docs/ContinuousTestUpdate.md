# ContinuousTestUpdate
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **String** |  | [default to null]
**ScheduleType** | [**ContinuousScheduleType**](ContinuousScheduleType.md) |  | [default to null]
**IntervalInMinutes** | **Int32** | Schedule interval in minutes | [default to null]
**NumberOfSessions** | **Int32** | Number of sessions | [default to null]
**TakeScriptScreenshots** | **Boolean** | Enable script screenshots | [default to null]
**RepeatCount** | **Int32** | Number of times the schedule is repeated | [default to null]
**IsRepeatEnabled** | **Boolean** | Enable schedule repeating | [default to null]
**IsEnabled** | **Boolean** | Enable schedule | [default to null]
**RestartOnComplete** | **Boolean** | Enable restarting on completion | [default to null]
**Name** | **String** | Test name | [default to null]
**Description** | **String** | Test description | [optional] [default to null]
**EnvironmentUpdate** | [**EnvironmentUpdate**](EnvironmentUpdate.md) |  | [optional] [default to null]
**Steps** | [**OneOfAppInvocationCreateDelayCreate[]**](OneOfAppInvocationCreateDelayCreate.md) | Workload steps creation data | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-PSOpenAPIToolsContinuousTestUpdate  -Type null `
 -ScheduleType null `
 -IntervalInMinutes null `
 -NumberOfSessions null `
 -TakeScriptScreenshots null `
 -RepeatCount null `
 -IsRepeatEnabled null `
 -IsEnabled null `
 -RestartOnComplete null `
 -Name null `
 -Description null `
 -EnvironmentUpdate null `
 -Steps null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

