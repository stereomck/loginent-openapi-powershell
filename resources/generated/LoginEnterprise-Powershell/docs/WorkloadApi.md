# PSOpenAPITools.PSOpenAPITools\Api.WorkloadApi

All URIs are relative to *http://localhost/publicApi*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Invoke-ConfigurationCreateTestWorkloadSteps**](WorkloadApi.md#Invoke-ConfigurationCreateTestWorkloadSteps) | **POST** /v4/tests/{testId}/workload | Add test-workload steps
[**Invoke-ConfigurationDeleteTestWorkloadStep**](WorkloadApi.md#Invoke-ConfigurationDeleteTestWorkloadStep) | **DELETE** /v4/tests/{testId}/workload/{stepId} | Remove test-workload step
[**Invoke-ConfigurationDeleteTestWorkloadSteps**](WorkloadApi.md#Invoke-ConfigurationDeleteTestWorkloadSteps) | **DELETE** /v4/tests/{testId}/workload | Remove test-workload steps
[**Invoke-ConfigurationGetTestWorkload**](WorkloadApi.md#Invoke-ConfigurationGetTestWorkload) | **GET** /v4/tests/{testId}/workload | Get test-workload by test id
[**Invoke-ConfigurationReplaceTestWorkloadSteps**](WorkloadApi.md#Invoke-ConfigurationReplaceTestWorkloadSteps) | **PUT** /v4/tests/{testId}/workload | Replace test-workload steps
[**Invoke-ConfigurationUpdateTestWorkloadStep**](WorkloadApi.md#Invoke-ConfigurationUpdateTestWorkloadStep) | **PUT** /v4/tests/{testId}/workload/{stepId} | Update test-workload step


<a name="Invoke-ConfigurationCreateTestWorkloadSteps"></a>
# **Invoke-ConfigurationCreateTestWorkloadSteps**
> void Invoke-ConfigurationCreateTestWorkloadSteps<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-OneOfAppInvocationCreateDelayCreate] <PSCustomObject[]><br>

Add test-workload steps

### Example
```powershell
Import-Module -Name PSOpenAPITools

$Configuration = Get-PSOpenAPIToolsConfiguration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)
$OneOfAppInvocationCreateDelayCreate = @("TODO") # OneOfAppInvocationCreateDelayCreate[] | List of step data

# Add test-workload steps
try {
    Invoke-ConfigurationCreateTestWorkloadSteps -TestId $TestId -OneOfAppInvocationCreateDelayCreate $OneOfAppInvocationCreateDelayCreate
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationCreateTestWorkloadSteps: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]
 **OneOfAppInvocationCreateDelayCreate** | [**OneOfAppInvocationCreateDelayCreate[]**](OneOfAppInvocationCreateDelayCreate.md)| List of step data | 

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationDeleteTestWorkloadStep"></a>
# **Invoke-ConfigurationDeleteTestWorkloadStep**
> void Invoke-ConfigurationDeleteTestWorkloadStep<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-StepId] <PSCustomObject><br>

Remove test-workload step

### Example
```powershell
Import-Module -Name PSOpenAPITools

$Configuration = Get-PSOpenAPIToolsConfiguration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)
$StepId = "StepId_example" # String | Step id (default to null)

# Remove test-workload step
try {
    Invoke-ConfigurationDeleteTestWorkloadStep -TestId $TestId -StepId $StepId
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationDeleteTestWorkloadStep: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]
 **StepId** | [**String**](String.md)| Step id | [default to null]

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationDeleteTestWorkloadSteps"></a>
# **Invoke-ConfigurationDeleteTestWorkloadSteps**
> void Invoke-ConfigurationDeleteTestWorkloadSteps<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-RequestBody] <String[]><br>

Remove test-workload steps

### Example
```powershell
Import-Module -Name PSOpenAPITools

$Configuration = Get-PSOpenAPIToolsConfiguration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)
$RequestBody = @("Property_example") # String[] | Step ids

# Remove test-workload steps
try {
    Invoke-ConfigurationDeleteTestWorkloadSteps -TestId $TestId -RequestBody $RequestBody
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationDeleteTestWorkloadSteps: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]
 **RequestBody** | [**String[]**](String.md)| Step ids | 

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationGetTestWorkload"></a>
# **Invoke-ConfigurationGetTestWorkload**
> Workload Invoke-ConfigurationGetTestWorkload<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>

Get test-workload by test id

### Example
```powershell
Import-Module -Name PSOpenAPITools

$Configuration = Get-PSOpenAPIToolsConfiguration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)

# Get test-workload by test id
try {
    Workload $Result = Invoke-ConfigurationGetTestWorkload -TestId $TestId
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationGetTestWorkload: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]

### Return type

[**Workload**](Workload.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationReplaceTestWorkloadSteps"></a>
# **Invoke-ConfigurationReplaceTestWorkloadSteps**
> void Invoke-ConfigurationReplaceTestWorkloadSteps<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-OneOfAppInvocationCreateDelayCreate] <PSCustomObject[]><br>

Replace test-workload steps

### Example
```powershell
Import-Module -Name PSOpenAPITools

$Configuration = Get-PSOpenAPIToolsConfiguration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)
$OneOfAppInvocationCreateDelayCreate = @("TODO") # OneOfAppInvocationCreateDelayCreate[] | List of step data (optional)

# Replace test-workload steps
try {
    Invoke-ConfigurationReplaceTestWorkloadSteps -TestId $TestId -OneOfAppInvocationCreateDelayCreate $OneOfAppInvocationCreateDelayCreate
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationReplaceTestWorkloadSteps: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]
 **OneOfAppInvocationCreateDelayCreate** | [**OneOfAppInvocationCreateDelayCreate[]**](OneOfAppInvocationCreateDelayCreate.md)| List of step data | [optional] 

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationUpdateTestWorkloadStep"></a>
# **Invoke-ConfigurationUpdateTestWorkloadStep**
> void Invoke-ConfigurationUpdateTestWorkloadStep<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-TestId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-StepId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-UNKNOWNBASETYPE] <PSCustomObject><br>

Update test-workload step

### Example
```powershell
Import-Module -Name PSOpenAPITools

$Configuration = Get-PSOpenAPIToolsConfiguration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$TestId = "TestId_example" # String | Test id (default to null)
$StepId = "StepId_example" # String | Step id (default to null)
$UNKNOWNBASETYPE = TODO # UNKNOWN_BASE_TYPE | Step data

# Update test-workload step
try {
    Invoke-ConfigurationUpdateTestWorkloadStep -TestId $TestId -StepId $StepId -UNKNOWNBASETYPE $UNKNOWNBASETYPE
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationUpdateTestWorkloadStep: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **TestId** | [**String**](String.md)| Test id | [default to null]
 **StepId** | [**String**](String.md)| Step id | [default to null]
 **UNKNOWNBASETYPE** | [**UNKNOWN_BASE_TYPE**](UNKNOWN_BASE_TYPE.md)| Step data | 

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

