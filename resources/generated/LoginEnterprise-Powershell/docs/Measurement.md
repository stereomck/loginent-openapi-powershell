# Measurement
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MeasurementId** | **String** | Measurement id | [optional] [default to null]
**AppExecutionId** | **String** | App execution id | [optional] [default to null]
**ApplicationId** | **String** | Application id | [optional] [default to null]
**LauncherName** | **String** | Launcher name | [optional] [default to null]
**AccountId** | **String** | Account id | [optional] [default to null]
**TestRunId** | **String** | Test run id | [optional] [default to null]
**UserSessionId** | **String** | User session id | [optional] [default to null]
**Duration** | **Double** | Duration | [optional] [default to null]
**Timestamp** | **System.DateTime** | Measurement time-stamp | [optional] [default to null]
**Properties** | [**Property[]**](Property.md) | Measurement properties | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-PSOpenAPIToolsMeasurement  -MeasurementId null `
 -AppExecutionId null `
 -ApplicationId null `
 -LauncherName null `
 -AccountId null `
 -TestRunId null `
 -UserSessionId null `
 -Duration null `
 -Timestamp null `
 -Properties null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

