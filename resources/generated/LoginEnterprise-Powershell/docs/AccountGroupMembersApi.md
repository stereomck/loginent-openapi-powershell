# PSOpenAPITools.PSOpenAPITools\Api.AccountGroupMembersApi

All URIs are relative to *http://localhost/publicApi*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Invoke-ConfigurationAddAccountGroupMembers**](AccountGroupMembersApi.md#Invoke-ConfigurationAddAccountGroupMembers) | **POST** /v4/account-groups/{groupId}/members | Add members to account-group
[**Invoke-ConfigurationGetAccountGroupMembers**](AccountGroupMembersApi.md#Invoke-ConfigurationGetAccountGroupMembers) | **GET** /v4/account-groups/{groupId}/members | Get paginated list of account-group members
[**Invoke-ConfigurationRemoveAccountGroupMembers**](AccountGroupMembersApi.md#Invoke-ConfigurationRemoveAccountGroupMembers) | **DELETE** /v4/account-groups/{groupId}/members | Remove members from account-group
[**Invoke-ConfigurationSetAccountGroupMembers**](AccountGroupMembersApi.md#Invoke-ConfigurationSetAccountGroupMembers) | **PUT** /v4/account-groups/{groupId}/members | Replace members in account-group


<a name="Invoke-ConfigurationAddAccountGroupMembers"></a>
# **Invoke-ConfigurationAddAccountGroupMembers**
> void Invoke-ConfigurationAddAccountGroupMembers<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-GroupId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-RequestBody] <String[]><br>

Add members to account-group

### Example
```powershell
Import-Module -Name PSOpenAPITools

$Configuration = Get-PSOpenAPIToolsConfiguration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$GroupId = "GroupId_example" # String | Account-group id (default to null)
$RequestBody = @("Property_example") # String[] | Account ids

# Add members to account-group
try {
    Invoke-ConfigurationAddAccountGroupMembers -GroupId $GroupId -RequestBody $RequestBody
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationAddAccountGroupMembers: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **GroupId** | [**String**](String.md)| Account-group id | [default to null]
 **RequestBody** | [**String[]**](String.md)| Account ids | 

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationGetAccountGroupMembers"></a>
# **Invoke-ConfigurationGetAccountGroupMembers**
> AccountResultSet Invoke-ConfigurationGetAccountGroupMembers<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-GroupId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-OrderBy] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Direction] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Count] <Int32><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-Offset] <System.Nullable[Int32]><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-IncludeTotalCount] <System.Nullable[Boolean]><br>

Get paginated list of account-group members

### Example
```powershell
Import-Module -Name PSOpenAPITools

$Configuration = Get-PSOpenAPIToolsConfiguration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$GroupId = "GroupId_example" # String | Account-group id (default to null)
$OrderBy = (Initialize-AccountSortKey) # AccountSortKey | Sort key (default to null)
$Direction = (Initialize-SortOrder) # SortOrder | Sort direction (default to null)
$Count = 987 # Int32 | Number of records to return (default to null)
$Offset = 987 # Int32 | Start offset (optional) (default to 0)
$IncludeTotalCount = true # Boolean | Include total number of records (optional) (default to false)

# Get paginated list of account-group members
try {
    AccountResultSet $Result = Invoke-ConfigurationGetAccountGroupMembers -GroupId $GroupId -OrderBy $OrderBy -Direction $Direction -Count $Count -Offset $Offset -IncludeTotalCount $IncludeTotalCount
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationGetAccountGroupMembers: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **GroupId** | [**String**](String.md)| Account-group id | [default to null]
 **OrderBy** | [**AccountSortKey**](AccountSortKey.md)| Sort key | [default to null]
 **Direction** | [**SortOrder**](SortOrder.md)| Sort direction | [default to null]
 **Count** | **Int32**| Number of records to return | [default to null]
 **Offset** | **Int32**| Start offset | [optional] [default to 0]
 **IncludeTotalCount** | **Boolean**| Include total number of records | [optional] [default to false]

### Return type

[**AccountResultSet**](AccountResultSet.md)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationRemoveAccountGroupMembers"></a>
# **Invoke-ConfigurationRemoveAccountGroupMembers**
> void Invoke-ConfigurationRemoveAccountGroupMembers<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-GroupId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-RequestBody] <String[]><br>

Remove members from account-group

### Example
```powershell
Import-Module -Name PSOpenAPITools

$Configuration = Get-PSOpenAPIToolsConfiguration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$GroupId = "GroupId_example" # String | Account-group id (default to null)
$RequestBody = @("Property_example") # String[] | Account ids

# Remove members from account-group
try {
    Invoke-ConfigurationRemoveAccountGroupMembers -GroupId $GroupId -RequestBody $RequestBody
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationRemoveAccountGroupMembers: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **GroupId** | [**String**](String.md)| Account-group id | [default to null]
 **RequestBody** | [**String[]**](String.md)| Account ids | 

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="Invoke-ConfigurationSetAccountGroupMembers"></a>
# **Invoke-ConfigurationSetAccountGroupMembers**
> void Invoke-ConfigurationSetAccountGroupMembers<br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-GroupId] <PSCustomObject><br>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[-RequestBody] <String[]><br>

Replace members in account-group

### Example
```powershell
Import-Module -Name PSOpenAPITools

$Configuration = Get-PSOpenAPIToolsConfiguration
# Configure API key authorization: Bearer
$Configuration["ApiKey"]["Authorization"] = "YOUR_API_KEY"
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
#$Configuration["ApiKeyPrefix"]["Authorization"] = "Bearer"
# Configure OAuth2 access token for authorization: oauth2
$Configuration["AccessToken"] = "YOUR_ACCESS_TOKEN";

$GroupId = "GroupId_example" # String | Account-group id (default to null)
$RequestBody = @("Property_example") # String[] | Account ids

# Replace members in account-group
try {
    Invoke-ConfigurationSetAccountGroupMembers -GroupId $GroupId -RequestBody $RequestBody
} catch {
    Write-Host ("Exception occured when calling Invoke-ConfigurationSetAccountGroupMembers: {0}" -f ($_.ErrorDetails | ConvertFrom-Json))
    Write-Host ("Response headers: {0}" -f ($_.Exception.Response.Headers | ConvertTo-Json))
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **GroupId** | [**String**](String.md)| Account-group id | [default to null]
 **RequestBody** | [**String[]**](String.md)| Account ids | 

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer), [OpenIdConnect](../README.md#OpenIdConnect), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

