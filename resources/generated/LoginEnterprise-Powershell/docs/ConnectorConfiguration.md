# ConnectorConfiguration
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Connector** | [**Connector**](Connector.md) |  | [optional] [default to null]
**ConnectorParameterValues** | [**StringStringKeyValuePair[]**](StringStringKeyValuePair.md) | Connector parameters | [optional] [default to null]

## Examples

- Prepare the resource
```powershell
Initialize-PSOpenAPIToolsConnectorConfiguration  -Connector null `
 -ConnectorParameterValues null
```

- Convert the resource to JSON
```powershell
$ | Convert-ToJSON
```

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

